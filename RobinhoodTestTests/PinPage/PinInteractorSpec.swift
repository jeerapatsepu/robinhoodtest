//
//  PinInteractorSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 9/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class PinInteractorSpec: XCTestCase {

    // MARK: - Subject Under Test (SUT)
    var sut: PinInteractor!
    
    // MARK: - Test Doubles
    var presentationLogicSpy: PinPresentationLogicSpy!
    var workerSpy: PinWorkerSpy!
    var stub: PinServiceSpy!

    override func setUp() {
        super.setUp()
        
        setupInteractor()
        setupPresentationLogic()
        setupWorker()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        workerSpy = nil
        stub = nil
    }
    
    func testGetPinText() {
        // given
        let pin = "123"
        var tmpPin = pin
        
        // when
        sut.validatePinText(pin: tmpPin.removeFirst().description)
        sut.validatePinText(pin: tmpPin.removeFirst().description)
        sut.validatePinText(pin: tmpPin.removeFirst().description)
        let pinText = sut.getPinText()
        
        // then
        XCTAssertTrue(workerSpy.isGetUserPinCalled)
        XCTAssertTrue(pinText == pin)
    }
    
    func testClearPinText() {
        // given
        let pin = "123"
        var tmpPin = pin
        
        // when
        sut.validatePinText(pin: tmpPin.removeFirst().description)
        sut.validatePinText(pin: tmpPin.removeFirst().description)
        sut.validatePinText(pin: tmpPin.removeFirst().description)
        sut.clearPinText()
        
        // then
        XCTAssertTrue(sut.getPinText() == "")
    }
    
    func testValidatePinTextShouldSuccess() {
        // given
        let userPin = "123456"
        var tmpUserPin = userPin
        
        // when
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        
        // then
        XCTAssertTrue(presentationLogicSpy.isPresentValidatePinTextSuccessCalled)
    }
    
    func testValidatePinTextShouldFailWhenMaxDigit() {
        // given
        let userPin = "000000"
        var tmpUserPin = userPin
        
        // when
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        
        // then
        XCTAssertTrue(presentationLogicSpy.isPresentValidatePinTextFailCalled)
    }
    
    func testValidatePinTextShouldFailWhenPinNotMaxDigit() {
        // given
        let userPin = "00"
        var tmpUserPin = userPin
        
        // when
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        sut.validatePinText(pin: tmpUserPin.removeFirst().description)
        
        // then
        XCTAssertFalse(presentationLogicSpy.isPresentValidatePinTextFailCalled)
        XCTAssertFalse(presentationLogicSpy.isPresentValidatePinTextSuccessCalled)
    }
    
    // MARK: - Test Helpers
    func setupInteractor() {
        sut = PinInteractor()
    }
    
    func setupPresentationLogic() {
        presentationLogicSpy = PinPresentationLogicSpy()
        sut.presenter = presentationLogicSpy
    }
    
    func setupWorker() {
        stub = PinServiceSpy()
        workerSpy = PinWorkerSpy(with: stub)
        sut.worker = workerSpy
    }
}

// MARK: - PinInteractorSpec
extension PinInteractorSpec {
    class PinPresentationLogicSpy: PinPresenterInterface {
        // MARK: Spied Methods
        var isPresentValidatePinTextSuccessCalled = false
        func presentValidatePinTextSuccess() {
            isPresentValidatePinTextSuccessCalled = true
        }
        
        var isPresentValidatePinTextFailCalled = false
        func presentValidatePinTextFail(text: String) {
            isPresentValidatePinTextFailCalled = true
        }
        
        func startLoading() {
            
        }
        
        func stopLoading() {

        }
    }
    
    class PinWorkerSpy: PinWorker {
        // MARK: Spied Methods
        var isGetUserPinCalled = false
        override func getUserPin() -> String? {
            isGetUserPinCalled = true
            return super.getUserPin()
        }
    }
    
    class PinServiceSpy: PinService {
        // MARK: Spied Methods
        override func getUserPin() -> String? {
            return super.getUserPin()
        }
    }
}
