//
//  PinWorkerSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 9/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class PinWorkerSpec: XCTestCase {

    // MARK: - Subject Under Test (SUT)
    var sut: PinWorker!
    
    // MARK: - Test Doubles
    var stub: PinServiceSpy!
    
    override func setUp() {
        super.setUp()
        
        setupWorker()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        stub = nil
    }
    
    func testGetUserPinShouldSuccess() {
        // given
        let pin = "123456".hashWithSHA256()
        
        // when
        let pinText = sut.getUserPin() ?? ""
        
        // then
        XCTAssertTrue(pinText == pin)
    }
    
    func testGetUserPinShouldFail() {
        // given
        let pin = "457856".hashWithSHA256()
        
        // when
        let pinText = sut.getUserPin() ?? ""
        
        // then
        XCTAssertFalse(pinText == pin)
    }
    
    // MARK: - Test Helpers
    func setupWorker() {
        stub = PinServiceSpy()
        sut = PinWorker(with: stub)
    }
}

// MARK: - PinWorkerSpec
extension PinWorkerSpec {
    class PinServiceSpy: PinService {
        // MARK: Spied Methods
        override func getUserPin() -> String? {
            return super.getUserPin()
        }
    }
}
