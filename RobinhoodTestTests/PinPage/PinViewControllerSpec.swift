//
//  PinViewControllerSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class PinViewControllerSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: PinViewController!
    var window: UIWindow!
    
    // MARK: - Test Doubles
    var businessLogicSpy: PinBusinessLogicSpy!
    var routerSpy: PinRouterSpy!
    
    override func setUp() {
        super.setUp()
        
        window = UIWindow()
        setupViewController()
        setupBusinessLogic()
        setupRouter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        window = nil
    }
    
    // MARK: IBActions/Delegates
    func testPinButtonShouldValidatePin() {
        // given
        loadView()
        
        // when
        sut.pinNumberButtonList.first?.sendActions(for: .touchUpInside)
        
        // then
        XCTAssertTrue(businessLogicSpy.isValidatePinTextCalled)
    }
    
    func testPinButtonShouldGetPin() {
        // given
        loadView()
        
        // when
        sut.pinNumberButtonList.first?.sendActions(for: .touchUpInside)
        
        // then
        XCTAssertTrue(businessLogicSpy.isGetPinTextCalled)
    }
    
    // MARK: Display Logic
    func testDisplayValidatePinTextSuccess() {
        // given
        loadView()
        
        // when
        sut.isOpenAsModalPage = true
        sut.displayValidatePinTextSuccess()
        
        // then
        XCTAssertTrue(routerSpy.isnavigateToRootViewControllerCalled)
        XCTAssertTrue(businessLogicSpy.isClearPinTextCalled)
    }
    
    func testDisplayValidatePinTextFail() {
        // given
        loadView()
        let failMessage = "Fail!"
        
        // when
        sut.displayValidatePinTextFail(text: failMessage)
        
        // then
        XCTAssertTrue((sut.titleLabel.text ?? "") == failMessage)
        XCTAssertTrue(businessLogicSpy.isClearPinTextCalled)
    }
    
    // MARK: - Test Helpers
    func setupViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Pin", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "PinView") as? PinViewController
    }
    
    func setupBusinessLogic() {
        businessLogicSpy = PinBusinessLogicSpy()
        sut.interactor = businessLogicSpy
    }
    
    func setupRouter() {
        routerSpy = PinRouterSpy()
        sut.router = routerSpy
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
}

extension PinViewControllerSpec {
    class PinBusinessLogicSpy: PinInteractorBusinessLogic {
        
        var isGetPinTextCalled = false
        func getPinText() -> String {
            isGetPinTextCalled = true
            return ""
        }
        
        var isClearPinTextCalled = false
        func clearPinText() {
            isClearPinTextCalled = true
        }
        
        var isValidatePinTextCalled = false
        func validatePinText(pin: String) {
            isValidatePinTextCalled = true
        }
    }
    
    class PinRouterSpy: PinRouterInterface {
        // MARK: Spied Methods
        var isnavigateToRootViewControllerCalled = false
        func navigateToRootViewController() {
            isnavigateToRootViewControllerCalled = true
        }
    }
}
