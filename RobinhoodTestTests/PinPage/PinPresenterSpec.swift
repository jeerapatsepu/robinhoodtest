//
//  PinPresenterSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 9/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class PinPresenterSpec: XCTestCase {

    // MARK: - Subject Under Test (SUT)
    var sut: PinPresenter!
    
    // MARK: - Test Doubles
    var pinView: PinViewSpy!

    override func setUp() {
        super.setUp()
        
        setupViewController()
        setupPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        pinView = nil
    }
    
    func testPresentValidatePinTextSuccess() {
        // given
        
        // when
        sut.presentValidatePinTextSuccess()
        
        // then
        XCTAssertTrue(pinView.isDisplayValidatePinTextSuccessCalled)
    }
    
    func testPresentValidatePinTextFail() {
        // given
        let message = "Fail!"
        
        // when
        sut.presentValidatePinTextFail(text: message)
        
        // then
        XCTAssertTrue(pinView.isDisplayValidatePinTextFailCalled)
    }
    
    func testStartLoading() {
        // given
        
        // when
        sut.startLoading()
        
        // then
        XCTAssertTrue(pinView.isStartLoadingCalled)
    }
    
    func testStopLoading() {
        // given
        
        // when
        sut.stopLoading()
        
        // then
        XCTAssertTrue(pinView.isStopLoadingCalled)
    }
    
    // MARK: - Test Helpers
    func setupViewController() {
        pinView = PinViewSpy()
    }
    
    func setupPresenter() {
        sut = PinPresenter()
        sut.viewController = pinView
    }
}

extension PinPresenterSpec {
    class PinViewSpy: PinView {
        // MARK: Spied Methods
        var isDisplayValidatePinTextSuccessCalled = false
        func displayValidatePinTextSuccess() {
            isDisplayValidatePinTextSuccessCalled = true
        }
        
        var isDisplayValidatePinTextFailCalled = false
        func displayValidatePinTextFail(text: String) {
            isDisplayValidatePinTextFailCalled = true
        }
        
        var isStartLoadingCalled = false
        func startLoading() {
            isStartLoadingCalled = true
        }
        
        var isStopLoadingCalled = false
        func stopLoading() {
            isStopLoadingCalled = true
        }
        
        func isEqual(_ object: Any?) -> Bool {
            return false
        }
        
        var hash: Int = 0
        
        var superclass: AnyClass?
        
        func `self`() -> Self {
            return self
        }
        
        func perform(_ aSelector: Selector!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func perform(_ aSelector: Selector!, with object: Any!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func perform(_ aSelector: Selector!, with object1: Any!, with object2: Any!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func isProxy() -> Bool {
            return false
        }
        
        func isKind(of aClass: AnyClass) -> Bool {
            return false
        }
        
        func isMember(of aClass: AnyClass) -> Bool {
            return false
        }
        
        func conforms(to aProtocol: Protocol) -> Bool {
            return false
        }
        
        func responds(to aSelector: Selector!) -> Bool {
            return false
        }
        
        var description: String = ""
    }
}
