//
//  CreateTaskViewControllerSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 9/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class CreateTaskViewControllerSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: CreateTaskViewController!
    var window: UIWindow!
    
    // MARK: - Test Doubles
    var businessLogicSpy: CreateTaskBusinessLogicSpy!
    var routerSpy: CreateTaskRouterSpy!
    
    override func setUp() {
        super.setUp()
        
        window = UIWindow()
        setupViewController()
        setupBusinessLogic()
        setupRouter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        window = nil
        businessLogicSpy = nil
        routerSpy = nil
    }
    
    // MARK: IBActions/Delegates
    func testCreateTaskButton() {
        // given
        loadView()
        let button = sut.navigationItem.rightBarButtonItem?.customView as! UIButton
        
        // when
        button.sendActions(for: .touchUpInside)
        
        // then
        XCTAssertTrue(businessLogicSpy.isCreateTaskCalled)
    }
    
    func testNavigateToRootViewControlleButton() {
        // given
        loadView()
        let navigateToRootViewControllerButton = sut.navigationItem.leftBarButtonItem?.customView as! UIButton
        
        // when
        navigateToRootViewControllerButton.sendActions(for: .touchUpInside)
        
        // then
        XCTAssertTrue(routerSpy.isnavigateToRootViewControllerCalled)
    }
    
    // MARK: Display Logic
    func testDisplayCreateTaskSuccess() {
        // given
        loadView()
        let taskItem = TaskManagementModels.ViewModel.TaskItem(
            id: "",
            iconText: "",
            iconBackground: .backgroundBlue,
            title: "",
            description: "",
            date: ""
        )
        
        // when
        sut.displayCreateTaskSuccess(display: taskItem)
        
        // then
        XCTAssertTrue(routerSpy.isnavigateToRootViewControllerCalled)
    }
    
    // MARK: - Test Helpers
    func setupViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "CreateTask", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "CreateTaskView") as? CreateTaskViewController
    }
    
    func setupBusinessLogic() {
        businessLogicSpy = CreateTaskBusinessLogicSpy()
        sut.interactor = businessLogicSpy
    }
    
    func setupRouter() {
        routerSpy = CreateTaskRouterSpy()
        sut.router = routerSpy
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
}

extension CreateTaskViewControllerSpec {
    class CreateTaskBusinessLogicSpy: CreateTaskInteractorBusinessLogic {
        // MARK: Spied Methods
        var isCreateTaskCalled = false
        func createTask(title: String, description: String) {
            isCreateTaskCalled = true
        }
    }
    
    class CreateTaskRouterSpy: CreateTaskRouterInterface {
        // MARK: Spied Methods
        var isnavigateToRootViewControllerCalled = false
        func navigateToRootViewController() {
            isnavigateToRootViewControllerCalled = true
        }
    }
}
