//
//  CreateTaskInteractorSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 10/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class CreateTaskInteractorSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: CreateTaskInteractor!
    
    // MARK: - Test Doubles
    var presentationLogicSpy: CreateTaskPresentationLogicSpy!

    override func setUp() {
        super.setUp()
        
        setupInteractor()
        setupPresentationLogic()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        presentationLogicSpy = nil
    }
    
    func testCreateTaskShouldSuccess() {
        // given
        let title = "t"
        let description = "d"
        
        // when
        sut.createTask(title: title, description: description)
        
        // then
        XCTAssertTrue(presentationLogicSpy.isPresentCreateTaskSuccessCalled)
    }
    
    func testCreateTaskShouldFail() {
        // given
        let title = ""
        let description = ""
        
        // when
        sut.createTask(title: title, description: description)
        
        // then
        XCTAssertFalse(presentationLogicSpy.isPresentCreateTaskSuccessCalled)
    }
    
    // MARK: - Test Helpers
    func setupInteractor() {
        sut = CreateTaskInteractor()
    }
    
    func setupPresentationLogic() {
        presentationLogicSpy = CreateTaskPresentationLogicSpy()
        sut.presenter = presentationLogicSpy
    }
}

// MARK: - CreateTaskInteractorSpec
extension CreateTaskInteractorSpec {
    class CreateTaskPresentationLogicSpy: CreateTaskPresenterInterface {
        // MARK: Spied Methods
        var isPresentCreateTaskSuccessCalled = false
        func presentCreateTaskSuccess(title: String, description: String) {
            isPresentCreateTaskSuccessCalled = true
        }
        
        func startLoading() {
            
        }
        
        func stopLoading() {
            
        }
    }
}
