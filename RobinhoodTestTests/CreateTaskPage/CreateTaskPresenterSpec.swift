//
//  CreateTaskPresenterSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 10/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class CreateTaskPresenterSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: CreateTaskPresenter!
    
    // MARK: - Test Doubles
    var viewSpy: CreateTaskViewSpy!

    override func setUp() {
        super.setUp()
        
        setupViewController()
        setupPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        viewSpy = nil
    }
    
    func testPresentCreateTaskSuccess() {
        // given
        let title = "testTitle"
        let description = "testDescription"
        
        // when
        sut.presentCreateTaskSuccess(title: title, description: description)
        
        // then
        XCTAssertTrue(viewSpy.isDisplayCreateTaskSuccessCalled)
    }
    
    func testStartLoading() {
        // given
        
        // when
        sut.startLoading()
        
        // then
        XCTAssertTrue(viewSpy.isStartLoadingCalled)
    }
    
    func testStopLoading() {
        // given
        
        // when
        sut.stopLoading()
        
        // then
        XCTAssertTrue(viewSpy.isStopLoadingCalled)
    }

    // MARK: - Test Helpers
    func setupViewController() {
        viewSpy = CreateTaskViewSpy()
    }
    
    func setupPresenter() {
        sut = CreateTaskPresenter()
        sut.viewController = viewSpy
    }
}

extension CreateTaskPresenterSpec {
    class CreateTaskViewSpy: CreateTaskView {
        // MARK: Spied Methods
        var isDisplayCreateTaskSuccessCalled = false
        func displayCreateTaskSuccess(display: RobinhoodTest.TaskManagementModels.ViewModel.TaskItem) {
            isDisplayCreateTaskSuccessCalled = true
        }
        
        var isStartLoadingCalled = false
        func startLoading() {
            isStartLoadingCalled = true
        }
        
        var isStopLoadingCalled = false
        func stopLoading() {
            isStopLoadingCalled = true
        }
        
        func isEqual(_ object: Any?) -> Bool {
            return false
        }
        
        var hash: Int = 0
        
        var superclass: AnyClass?
        
        func `self`() -> Self {
            return self
        }
        
        func perform(_ aSelector: Selector!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func perform(_ aSelector: Selector!, with object: Any!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func perform(_ aSelector: Selector!, with object1: Any!, with object2: Any!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func isProxy() -> Bool {
            return false
        }
        
        func isKind(of aClass: AnyClass) -> Bool {
            return false
        }
        
        func isMember(of aClass: AnyClass) -> Bool {
            return false
        }
        
        func conforms(to aProtocol: Protocol) -> Bool {
            return false
        }
        
        func responds(to aSelector: Selector!) -> Bool {
            return false
        }
        
        var description: String = ""
    }
}
