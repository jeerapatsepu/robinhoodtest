//
//  TaskManagementPresenterSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 10/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class TaskManagementPresenterSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: TaskManagementPresenter!
    
    // MARK: - Test Doubles
    var viewSpy: TaskManagementViewSpy!

    override func setUp() {
        super.setUp()
        
        setupViewController()
        setupPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        viewSpy = nil
    }
    
    func testPresentTaskListSuccess() {
        // given
        let response: [TaskManagementModels.Response.TaskItem] = []
        let taskList: [TaskManagementModels.ViewModel.TaskListViewModel] = []
        
        // when
        sut.presentTaskListSuccess(response: response, taskList: taskList)
        
        // then
        XCTAssertTrue(viewSpy.isDisplayTaskListSuccessCalled)
    }
    
    func testPresentTaskListWithLoadMoreSuccess() {
        // given
        let response: [TaskManagementModels.Response.TaskItem] = []
        let taskList: [TaskManagementModels.ViewModel.TaskListViewModel] = []
        
        // when
        sut.presentTaskListWithLoadMoreSuccess(response: response, taskList: taskList)
        
        // then
        XCTAssertTrue(viewSpy.isDisplayTaskListSuccessCalled)
    }
    
    func testPresentTaskListFail() {
        // given
        let message = "Please try again"
        
        // when
        sut.presentTaskListFail(text: message)
        
        // then
        XCTAssertTrue(viewSpy.isDisplayTaskListFailCalled)
    }
    
    func testPresentRemoveItemSuccess() {
        // given
        let taskList = [
            TaskManagementModels.ViewModel.TaskListViewModel(dateText: "Today", taskList: [
                TaskManagementModels.ViewModel.TaskItem(id: "",
                                                        iconText: "",
                                                        iconBackground: .backgroundBlue,
                                                        title: "",
                                                        description: "",
                                                        date: "")
            ])
        ]
        
        // when
        sut.presentRemoveItemSuccess(taskList: taskList, indexPath: IndexPath(row: 0, section: 0))
        
        // then
        XCTAssertTrue(viewSpy.isDisplayRemoveItemSuccessCalled)
    }
    
    func testPresentAddItemSuccessShouldSuccess() {
        // given
        let taskList = [
            TaskManagementModels.ViewModel.TaskListViewModel(dateText: "Today", taskList: [
                TaskManagementModels.ViewModel.TaskItem(id: "",
                                                        iconText: "",
                                                        iconBackground: .backgroundBlue,
                                                        title: "",
                                                        description: "",
                                                        date: "")
            ])
        ]
        
        // when
        sut.presentAddItemSuccess(
            taskList: taskList,
            data: TaskManagementModels.ViewModel.TaskItem(id: "",
                                                          iconText: "",
                                                          iconBackground: .backgroundBlue,
                                                          title: "",
                                                          description: "",
                                                          date: "")
        )
        
        // then
        XCTAssertTrue(viewSpy.isDisplayAddItemSuccessCalled)
    }
    
    func testPresentAddItemSuccessShouldFail() {
        // given
        let taskList: [TaskManagementModels.ViewModel.TaskListViewModel] = []
        
        // when
        sut.presentAddItemSuccess(
            taskList: taskList,
            data: TaskManagementModels.ViewModel.TaskItem(id: "",
                                                          iconText: "",
                                                          iconBackground: .backgroundBlue,
                                                          title: "",
                                                          description: "",
                                                          date: "")
        )
        
        // then
        XCTAssertTrue(!viewSpy.isDisplayAddItemSuccessCalled)
    }
    
    func testStartLoading() {
        // given
        
        // when
        sut.startLoading()
        
        // then
        XCTAssertTrue(viewSpy.isStartLoadingCalled)
    }
    
    func testStopLoading() {
        // given
        
        // when
        sut.stopLoading()
        
        // then
        XCTAssertTrue(viewSpy.isStopLoadingCalled)
    }
    
    func testStartLoadMoreLoading() {
        // given
        
        // when
        sut.startLoadMoreLoading()
        
        // then
        XCTAssertTrue(viewSpy.isStartLoadMoreLoadingCalled)
    }
    
    func testStopLoadMoreLoading() {
        // given
        
        // when
        sut.stopLoadMoreLoading()
        
        // then
        XCTAssertTrue(viewSpy.isStopLoadMoreLoadingCalled)
    }
   
    // MARK: - Test Helpers
    func setupViewController() {
        viewSpy = TaskManagementViewSpy()
    }
    
    func setupPresenter() {
        sut = TaskManagementPresenter()
        sut.viewController = viewSpy
    }
}

extension TaskManagementPresenterSpec {
    class TaskManagementViewSpy: TaskManagementView {
        // MARK: Spied Methods
        var isDisplayTaskListSuccessCalled = false
        func displayTaskListSuccess(display: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isDisplayTaskListSuccessCalled = true
        }
        
        var isDisplayTaskListFailCalled = false
        func displayTaskListFail(text: String) {
            isDisplayTaskListFailCalled = true
        }
        
        var isDisplayRemoveItemSuccessCalled = false
        func displayRemoveItemSuccess(display: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isDisplayRemoveItemSuccessCalled = true
        }
        
        var isDisplayAddItemSuccessCalled = false
        func displayAddItemSuccess(display: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isDisplayAddItemSuccessCalled = true
        }
        
        var isStartLoadMoreLoadingCalled = false
        func startLoadMoreLoading() {
            isStartLoadMoreLoadingCalled = true
        }
        
        var isStopLoadMoreLoadingCalled = false
        func stopLoadMoreLoading() {
            isStopLoadMoreLoadingCalled = true
        }
        
        var isStartLoadingCalled = false
        func startLoading() {
            isStartLoadingCalled = true
        }
        
        var isStopLoadingCalled = false
        func stopLoading() {
            isStopLoadingCalled = true
        }
        
        func isEqual(_ object: Any?) -> Bool {
            return false
        }
        
        var hash: Int = 0
        
        var superclass: AnyClass?
        
        func `self`() -> Self {
            return self
        }
        
        func perform(_ aSelector: Selector!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func perform(_ aSelector: Selector!, with object: Any!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func perform(_ aSelector: Selector!, with object1: Any!, with object2: Any!) -> Unmanaged<AnyObject>! {
            return nil
        }
        
        func isProxy() -> Bool {
            return false
        }
        
        func isKind(of aClass: AnyClass) -> Bool {
            return false
        }
        
        func isMember(of aClass: AnyClass) -> Bool {
            return false
        }
        
        func conforms(to aProtocol: Protocol) -> Bool {
            return false
        }
        
        func responds(to aSelector: Selector!) -> Bool {
            return false
        }
        
        var description: String = ""
    }
}
