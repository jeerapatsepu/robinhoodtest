//
//  TaskManagementInteractorSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 9/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class TaskManagementInteractorSpec: XCTestCase {

    // MARK: - Subject Under Test (SUT)
    var sut: TaskManagementInteractor!
    
    // MARK: - Test Doubles
    var presentationLogicSpy: TaskManagementPresentationLogicSpy!
    var workerSpy: TaskManagementWorkerSpy!

    override func setUp() {
        super.setUp()
        
        setupInteractor()
        setupPresentationLogic()
        setupWorker()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        workerSpy = nil
    }
    
    func testFetchTaskList() {
        // given
        let taskStatus :TaskManagementModels.ViewModel.TaskStatusType = .toDo
        
        // when
        sut.fetchTaskList(taskStatus: taskStatus, taskList: [])
        
        // then
        XCTAssertTrue(workerSpy.isFetchTaskListCalled)
    }
    
    func testFetchTaskListWithLoadMore() {
        // given
        let taskStatus :TaskManagementModels.ViewModel.TaskStatusType = .toDo
        
        // when
        sut.fetchTaskListWithLoadMore(taskStatus: taskStatus, taskList: [])
        
        // then
        XCTAssertTrue(workerSpy.isFetchTaskListCalled)
    }
    
    func testRemoveItem() {
        // given
        let taskList = [TaskManagementModels.ViewModel.TaskListViewModel(
            dateText: "Today",
            taskList: [TaskManagementModels.ViewModel.TaskItem(
                id: "",
                iconText: "",
                iconBackground: .backgroundBlue,
                title: "",
                description: "",
                date: ""
            )]
        )]
        
        // when
        sut.removeItem(taskList: taskList, indexPath: IndexPath(row: 0, section: 0))
        
        // then
        XCTAssertTrue(presentationLogicSpy.isPresentRemoveItemSuccessCalled)
    }
    
    func testAddItem() {
        // given
        
        // when
        sut.addItem(
            taskList: [],
            data: TaskManagementModels.ViewModel.TaskItem(
                id: "",
                iconText: "",
                iconBackground: .backgroundBlue,
                title: "",
                description: "",
                date: ""
            ))
        
        // then
        XCTAssertTrue(presentationLogicSpy.isPresentAddItemSuccessCalled)
    }
    
    // MARK: - Test Helpers
    func setupInteractor() {
        sut = TaskManagementInteractor()
    }
    
    func setupPresentationLogic() {
        presentationLogicSpy = TaskManagementPresentationLogicSpy()
        sut.presenter = presentationLogicSpy
    }
    
    func setupWorker() {
        workerSpy = TaskManagementWorkerSpy()
        sut.worker = workerSpy
    }
}

// MARK: - TaskManagementInteractorSpec
extension TaskManagementInteractorSpec {
    class TaskManagementPresentationLogicSpy: TaskManagementPresenterInterface {
        // MARK: Spied Methods
        var isPresentTaskListSuccessCalled = false
        func presentTaskListSuccess(response: [RobinhoodTest.TaskManagementModels.Response.TaskItem], taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isPresentTaskListSuccessCalled = true
        }
        
        var isPresentTaskListWithLoadMoreSuccessCalled = false
        func presentTaskListWithLoadMoreSuccess(response: [RobinhoodTest.TaskManagementModels.Response.TaskItem], taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isPresentTaskListWithLoadMoreSuccessCalled = true
        }
        
        var isPresentTaskListFailCalled = false
        func presentTaskListFail(text: String) {
            isPresentTaskListFailCalled = true
        }
        
        var isPresentRemoveItemSuccessCalled = false
        func presentRemoveItemSuccess(taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel], indexPath: IndexPath) {
            isPresentRemoveItemSuccessCalled = true
        }
        
        var isPresentAddItemSuccessCalled = false
        func presentAddItemSuccess(taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel], data: RobinhoodTest.TaskManagementModels.ViewModel.TaskItem) {
            isPresentAddItemSuccessCalled = true
        }
        
        var isStartLoadingCalled = false
        func startLoading() {
            isStartLoadingCalled = true
        }
        
        var isStopLoadingCalled = false
        func stopLoading() {
            isStopLoadingCalled = true
        }
        
        var isStartLoadMoreLoadingCalled = false
        func startLoadMoreLoading() {
            isStartLoadMoreLoadingCalled = true
        }
        
        var isStopLoadMoreLoadingCalled = false
        func stopLoadMoreLoading() {
            isStopLoadMoreLoadingCalled = true
        }
    }
    
    class TaskManagementWorkerSpy: TaskManagementWorkerInterface {
        // MARK: Spied Methods
        var isFetchTaskListCalled = false
        func fetchTaskList(request: RobinhoodTest.TaskManagementModels.Request.TaskListRequest, successs: @escaping (RobinhoodTest.TaskManagementModels.Response.TaskListResponse?) -> Void, failure: @escaping (String) -> Void) {
            isFetchTaskListCalled = true
        }
    }
}
