//
//  TaskManagementWorkerSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 10/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class TaskManagementWorkerSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: TaskManagementWorker!
    
    // MARK: - Test Doubles
    var stub: TaskManagementServiceSpy!
    
    override func setUp() {
        super.setUp()
        
        setupWorker()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        stub = nil
    }
    
    func testFetchTaskList() {
        // given
        let request = TaskManagementModels.Request.TaskListRequest(offset: 0, limit: 10, sortBy: .ascending, isAsc: true, status: .toDo)
        
        // when
        sut.fetchTaskList(
            request: request,
            successs: { _ in
            }, 
            failure: { _ in
            }
        )
        
        // then
        XCTAssertTrue(stub.isFetchTaskListCalled)
    }
    
    // MARK: - Test Helpers
    func setupWorker() {
        stub = TaskManagementServiceSpy()
        sut = TaskManagementWorker(with: stub)
    }
}

extension TaskManagementWorkerSpec {
    class TaskManagementServiceSpy: TaskManagementService {
        // MARK: Spied Methods
        var isFetchTaskListCalled = false
        override func fetchTaskList(params: [String : String]?, success: @escaping (Data) -> Void, failure: @escaping (Error) -> Void) {
            isFetchTaskListCalled = true
        }
    }
}
