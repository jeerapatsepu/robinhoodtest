//
//  TaskManagementControllerSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 9/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class TaskManagementControllerSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: TaskManagementViewController!
    var window: UIWindow!
    
    // MARK: - Test Doubles
    var businessLogicSpy: TaskManagementInteractorBusinessLogicSpy!
    var routerSpy: TaskManagemenRouterSpy!
    
    override func setUp() {
        super.setUp()
        
        window = UIWindow()
        setupViewController()
        setupBusinessLogic()
        setupRouter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
        window = nil
        businessLogicSpy = nil
        routerSpy = nil
    }
    
    // MARK: View Lifecycle
    func testViewDidLoad() {
        // given
        loadView()
        let taskStatus: TaskManagementModels.ViewModel.TaskStatusType = .toDo
        
        // when
        businessLogicSpy.fetchTaskList(taskStatus: taskStatus, taskList: [])
        
        // then
        XCTAssertTrue(businessLogicSpy.isFetchTaskListCalled)
    }
    
    // MARK: IBActions/Delegates
    func testFetchTaskListWithLoadMore() {
        // given
        loadView()
        let displayViewModelList = [TaskManagementModels.ViewModel.TaskListViewModel(
            dateText: "Today",
            taskList: [
                TaskManagementModels.ViewModel.TaskItem(
                    id: "",
                    iconText: "",
                    iconBackground: .backgroundBlue,
                    title: "",
                    description: "",
                    date: ""
                )
            ]
        )]
        
        // when
        sut.displayTaskListSuccess(display: displayViewModelList)
        sut.scrollViewWillBeginDragging(sut.taskListTableView)
        sut.tableView(sut.taskListTableView, willDisplay: sut.taskListTableView.visibleCells.first ?? UITableViewCell(), forRowAt: IndexPath(row: 0, section: 0))
        
        // then
        XCTAssertTrue(businessLogicSpy.isFetchTaskListWithLoadMoreCalled)
    }
    
    func testRemoveItem() {
        // given
        loadView()
        let displayViewModelList = [TaskManagementModels.ViewModel.TaskListViewModel(
            dateText: "Today",
            taskList: [
                TaskManagementModels.ViewModel.TaskItem(
                    id: "",
                    iconText: "",
                    iconBackground: .backgroundBlue,
                    title: "",
                    description: "",
                    date: ""
                )
            ]
        )]
        
        // when
        sut.displayTaskListSuccess(display: displayViewModelList)
        businessLogicSpy.removeItem(taskList: displayViewModelList, indexPath: IndexPath(row: 0, section: 0))
        
        // then
        XCTAssertTrue(businessLogicSpy.isRemoveItemCalled)
    }
    
    func testAddItem() {
        // given
        loadView()
        let task = TaskManagementModels.ViewModel.TaskItem(
            id: "",
            iconText: "",
            iconBackground: .backgroundBlue,
            title: "",
            description: "",
            date: ""
        )
        
        // when
        sut.didCreateTaskSuccess(data: task)
        
        // then
        XCTAssertTrue(businessLogicSpy.isAddItemCalled)
    }
    
    func testNavigateToCreateTaskViewController() {
        // given
        loadView()
        
        // when
        sut.createTaskButton.sendActions(for: .touchUpInside)
        
        // then
        XCTAssertTrue(routerSpy.isNavigateToCreateTaskViewControllerCalled)
    }
    
    func testDidSelectRowAt() {
        // given
        loadView()
        let displayViewModelList = [TaskManagementModels.ViewModel.TaskListViewModel(
            dateText: "Today",
            taskList: [
                TaskManagementModels.ViewModel.TaskItem(
                    id: "",
                    iconText: "",
                    iconBackground: .backgroundBlue,
                    title: "",
                    description: "",
                    date: ""
                )
            ]
        )]
        
        // when
        sut.displayTaskListSuccess(display: displayViewModelList)
        sut.tableView(sut.taskListTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        // then
        XCTAssertTrue(routerSpy.isNavigateToTaskDetailViewControllerCalled)
    }
    
    func testDisplayTaskListSuccess() {
        // given
        loadView()
        let taskList = [
            TaskManagementModels.ViewModel.TaskListViewModel(dateText: "Today", taskList: [
                TaskManagementModels.ViewModel.TaskItem(id: "",
                                                        iconText: "",
                                                        iconBackground: .backgroundBlue,
                                                        title: "",
                                                        description: "",
                                                        date: "")
            ])
        ]
        
        // when
        sut.displayTaskListSuccess(display: taskList)
        
        // then
        XCTAssertTrue(!sut.taskListTableView.visibleCells.isEmpty)
    }
    
    func testDisplayTaskListFail() {
        // given
        loadView()
        let message = "Please try again"
        
        // when
        sut.displayTaskListFail(text: message)
        
        // then
        XCTAssertTrue(sut.presentedViewController is UIAlertController)
    }
    
    func testDisplayRemoveItemSuccess() {
        // given
        loadView()
        let taskList: [TaskManagementModels.ViewModel.TaskListViewModel] = []
        
        // when
        sut.displayRemoveItemSuccess(display: taskList)
        
        // then
        XCTAssertTrue(sut.taskListTableView.visibleCells.isEmpty)
    }
    
    func testDisplayAddItemSuccess() {
        // given
        loadView()
        let taskList = [
            TaskManagementModels.ViewModel.TaskListViewModel(dateText: "Today", taskList: [
                TaskManagementModels.ViewModel.TaskItem(id: "",
                                                        iconText: "",
                                                        iconBackground: .backgroundBlue,
                                                        title: "",
                                                        description: "",
                                                        date: "")
            ])
        ]
        
        // when
        sut.displayAddItemSuccess(display: taskList)
        
        // then
        XCTAssertTrue(!sut.taskListTableView.visibleCells.isEmpty)
        XCTAssertTrue(sut.taskListTableView.contentOffset == .zero)
    }
    
    func testStopLoading() {
        // given
        loadView()
        
        // when
        sut.stopLoading()
        
        // then
        XCTAssertTrue(!(sut.taskListTableView.refreshControl?.isRefreshing ?? true))
    }
    
    func testStartLoadMoreLoading() {
        // given
        loadView()
        
        // when
        sut.startLoadMoreLoading()
        
        // then
        XCTAssertTrue(sut.loadMoreView.isAnimating)
    }
    
    func testStopLoadMoreLoading() {
        // given
        loadView()
        
        // when
        sut.stopLoadMoreLoading()
        
        // then
        XCTAssertTrue(!sut.loadMoreView.isAnimating)
    }
    
    // MARK: - Test Helpers
    func setupViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "TaskManagement", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "TaskManagementView") as? TaskManagementViewController
    }
    
    func setupBusinessLogic() {
        businessLogicSpy = TaskManagementInteractorBusinessLogicSpy()
        sut.interactor = businessLogicSpy
    }
    
    func setupRouter() {
        routerSpy = TaskManagemenRouterSpy()
        sut.router = routerSpy
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
}

extension TaskManagementControllerSpec {
    class TaskManagementInteractorBusinessLogicSpy: TaskManagementInteractorBusinessLogic {
        // MARK: Spied Methods
        var isFetchTaskListCalled = false
        func fetchTaskList(taskStatus: RobinhoodTest.TaskManagementModels.ViewModel.TaskStatusType, taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isFetchTaskListCalled = true
        }
        
        var isFetchTaskListWithLoadMoreCalled = false
        func fetchTaskListWithLoadMore(taskStatus: RobinhoodTest.TaskManagementModels.ViewModel.TaskStatusType, taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel]) {
            isFetchTaskListWithLoadMoreCalled = true
        }
        
        var isRemoveItemCalled = false
        func removeItem(taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel], indexPath: IndexPath) {
            isRemoveItemCalled = true
        }
        
        var isAddItemCalled = false
        func addItem(taskList: [RobinhoodTest.TaskManagementModels.ViewModel.TaskListViewModel], data: RobinhoodTest.TaskManagementModels.ViewModel.TaskItem) {
            isAddItemCalled = true
        }
    }
    
    class TaskManagemenRouterSpy: TaskManagementRouterInterface {        
        // MARK: Spied Methods
        var isNavigateToCreateTaskViewControllerCalled = false
        func navigateToCreateTaskViewController() {
            isNavigateToCreateTaskViewControllerCalled = true
        }
        
        var isNavigateToTaskDetailViewControllerCalled = false
        func navigateToTaskDetailViewController(title: String, description: String, iconBackgroundColor: UIColor) {
            isNavigateToTaskDetailViewControllerCalled = true
        }
    }
}
