//
//  TaskDetailViewControllerSpec.swift
//  RobinhoodTestTests
//
//  Created by Jeerapat Sripumngoen on 10/11/2566 BE.
//

import XCTest
@testable import RobinhoodTest

final class TaskDetailViewControllerSpec: XCTestCase {
    // MARK: - Subject Under Test (SUT)
    var sut: TaskDetailViewController!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        
        window = UIWindow()
        setupViewController()
    }
    
    override func tearDown() {
        super.tearDown()
        
        window = nil
    }
    
    // MARK: View Lifecycle
    func testViewDidLoad() {
        // given
        sut.taskTitle = "Title"
        sut.taskDescription = "Description"
        sut.taskIconBackgroundColor = .backgroundBlue
        loadView()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssertTrue((sut.titleLabel.text ?? "") == sut.taskTitle)
        XCTAssertTrue((sut.descriptionLabel.text ?? "") == sut.taskDescription)
        XCTAssertTrue(sut.iconBackground.backgroundColor == sut.taskIconBackgroundColor)
    }

    // MARK: - Test Helpers
    func setupViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "TaskDetail", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "TaskDetailView") as? TaskDetailViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
}
