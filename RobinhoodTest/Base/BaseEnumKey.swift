//
//  BaseEnumKey.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import Foundation

enum UserDefaultsKey: String {
    case userProfileImage = "userProfileImage"
    case userProfileName = "userProfileName"
    case appInLastForegroundTimeStamp = "appInLastForegroundTimeStamp"
}

enum KeychainKey: String {
    case userProfilePin = "userProfilePin"
}

enum NotificationNameKey: String {
    case validateUserPinSuccess = "validateUserPinSuccess"
    case userInactive = "userInactive"
}
