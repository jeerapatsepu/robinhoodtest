//
//  BaseNavigationController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 30/10/2566 BE.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupView()
    }
    
    private func setupView() {
        let barAppearance = UINavigationBarAppearance()
        barAppearance.configureWithOpaqueBackground()
        barAppearance.backgroundColor = UIColor.baseNavigationBar
        let backImage = UIImage(named: "ic-back-black32")?.withRenderingMode(.alwaysOriginal)
        barAppearance.setBackIndicatorImage(backImage, transitionMaskImage: backImage)
        
        barAppearance.backButtonAppearance = UIBarButtonItemAppearance(style: .plain)
        barAppearance.shadowColor = nil
        barAppearance.shadowImage = nil
        navigationBar.scrollEdgeAppearance = barAppearance
        navigationBar.standardAppearance = barAppearance
    }
}
