//
//  BaseWindow.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 5/11/2566 BE.
//

import UIKit

class BaseWindow: UIWindow {
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        
        if let allTouches = event.allTouches, let phase = allTouches.first?.phase, phase == .began {
            print("beginTouch")
            startTimer()
        }
    }
    
    private func startTimer() {
        let topViewController = BaseTool.getTopViewController()
        switch topViewController {
            case is MainViewController:
                guard let mainViewController = topViewController as? MainViewController, mainViewController.currentPageIndex != 0 else { return }
                PINStateManager.shared.startTimer()
                
            case is PinViewController:
                break
                
            default:
                PINStateManager.shared.startTimer()
        }
    }
}
