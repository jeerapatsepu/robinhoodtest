//
//  BaseView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 30/10/2566 BE.
//

import UIKit

protocol BaseView: NSObjectProtocol {
    func startLoading()
    func stopLoading()
}
