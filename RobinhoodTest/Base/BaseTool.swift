//
//  BaseTool.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import UIKit

final class BaseTool {
    private init() {
        
    }
    
    static func getWindowScene() -> UIWindowScene? {
        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
           return windowScene
        }
        return nil
    }
    
    static func getWindow() -> UIWindow? {
        var window: UIWindow?
        if let windowScene = getWindowScene() {
            window = windowScene.keyWindow
            if window == nil {
                window = windowScene.windows.first
            }
        }
        return window
    }
    
    static func getTopViewController() -> UIViewController? {
        guard let window = getWindow(), var rootViewController = window.rootViewController else {
            return nil
        }
        
        if let rootNewController = rootViewController as? UINavigationController, let lastViewController = rootNewController.viewControllers.last {
            rootViewController = lastViewController
        }
        
        while let presentedViewController = rootViewController.presentedViewController {
            rootViewController = presentedViewController
        }
        
        if let rootNewController = rootViewController as? UINavigationController, let lastViewController = rootNewController.viewControllers.last {
            rootViewController = lastViewController
        }
        
        return rootViewController
    }
    
    static func getBottomSafeAreaHeight() -> CGFloat {
        if let window = getWindow() {
            return window.safeAreaInsets.bottom
        }
        return 0.0
    }
    
    static func getDateFormatter(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ") -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter
    }
    
    static func getCalendar() -> Calendar {
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar
    }
    
    static func randomString(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }
}
