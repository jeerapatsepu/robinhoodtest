//
//  BaseViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 30/10/2566 BE.
//

import Kingfisher
import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBackButtonTitleIfNeeded()
    }
    
    private func setupBackButtonTitleIfNeeded() {
        guard navigationController != nil else { return }
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButton
    }
    
    func setupRightProfileView() {
        guard navigationController != nil else { return }
        let defaultProfileImage = UIImage(named: "ic-default-profile32")?.withRenderingMode(.alwaysOriginal)
        
        let profileImageView = UIImageView()
        profileImageView.roundWithBorder(cornerRadius: 16.0, borderColor: .baseBorderColor)
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            profileImageView.widthAnchor.constraint(equalToConstant: 32),
            profileImageView.heightAnchor.constraint(equalToConstant: 32)
        ])
        
        if let imageProfileUrl = UserAuthentication.shared.getUserProfileImageUrl(), !imageProfileUrl.isEmpty {
            profileImageView.contentMode = .scaleAspectFill
            profileImageView.setImage(with: imageProfileUrl, placeholder: defaultProfileImage)
        } else {
            profileImageView.contentMode = .scaleAspectFit
            profileImageView.image = defaultProfileImage
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: profileImageView)
    }
    
    func setupRightDismissButton() {
        guard navigationController != nil else { return }
        let dismissImage = UIImage(named: "ic-dismiss-white32")?.withRenderingMode(.alwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: dismissImage, style: .plain, target: self, action: #selector(tapDismissImageButton))
    }
    
    func setupTapViewToCancelKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func tapDismissImageButton() {
        dismiss(animated: true)
    }
    
    @objc private func tapView() {
        view.endEditing(true)
    }
}
