//
//  TodoStatusHeaderView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import UIKit

protocol TodoStatusHeaderViewDelegate: NSObjectProtocol {
    func didTapTodoStatusHeaderButton(type: TaskManagementModels.ViewModel.TaskStatusType)
}

class TodoStatusHeaderView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var toDoButton: TodoStatusHeaderButton!
    @IBOutlet weak var doingButton: TodoStatusHeaderButton!
    @IBOutlet weak var doneButton: TodoStatusHeaderButton!
    
    private lazy var buttonWidth = getButtonWidth()
    
    weak var delegate: TodoStatusHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupBackgroundView()
        setupToDoButton()
        setupDoingButton()
        setupDoneButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        setupBackgroundView()
        setupToDoButton()
        setupDoingButton()
        setupDoneButton()
    }
    
    private func setupView() {
        Bundle(for: TodoStatusHeaderView.self).loadNibNamed("TodoStatusHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    private func setupBackgroundView() {
        backgroundView.backgroundColor = .backgroundGray
        backgroundView.layer.cornerRadius = 24.0
    }
    
    private func setupToDoButton() {
        UIView.performWithoutAnimation {
            toDoButton.setTitle("To-do", for: .normal)
            toDoButton.layoutIfNeeded()
        }
        
        toDoButton.buttonWidth = buttonWidth
        toDoButton.buttonHeight = 32.0
        toDoButton.addTarget(self, action: #selector(tapToDoButton), for: .touchUpInside)
    }
    
    private func setupDoingButton() {
        UIView.performWithoutAnimation {
            doingButton.setTitle("Doing", for: .normal)
            doingButton.layoutIfNeeded()
        }
        
        doingButton.buttonWidth = buttonWidth
        doingButton.buttonHeight = 32.0
        doingButton.addTarget(self, action: #selector(tapDoingButton), for: .touchUpInside)
    }
    
    private func setupDoneButton() {
        UIView.performWithoutAnimation {
            doneButton.setTitle("Done", for: .normal)
            doneButton.layoutIfNeeded()
        }
        
        doneButton.buttonWidth = buttonWidth
        doneButton.buttonHeight = 32.0
        doneButton.addTarget(self, action: #selector(tapDoneButton), for: .touchUpInside)
    }
    
    private func getButtonWidth() -> CGFloat {
        let backgroundViewWidth = UIScreen.main.bounds.width - 96.0 // 32+8+8+32 + pading (8+8)
        return backgroundViewWidth / 3.0
    }
    
    func setButtonActive(type: TaskManagementModels.ViewModel.TaskStatusType) {
        toDoButton.setButtonInActive()
        doingButton.setButtonInActive()
        doneButton.setButtonInActive()
        
        switch type {
            case .toDo:
                toDoButton.setButtonActive()
                
            case .doing:
                doingButton.setButtonActive()
                
            case .done:
                doneButton.setButtonActive()
        }
    }
    
    @objc private func tapToDoButton() {
        delegate?.didTapTodoStatusHeaderButton(type: .toDo)
    }
    
    @objc private func tapDoingButton() {
        delegate?.didTapTodoStatusHeaderButton(type: .doing)
    }
    
    @objc private func tapDoneButton() {
        delegate?.didTapTodoStatusHeaderButton(type: .done)
    }
}
