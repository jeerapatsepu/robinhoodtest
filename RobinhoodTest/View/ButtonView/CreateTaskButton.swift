//
//  CreateTaskButton.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

class CreateTaskButton: UIButton {
    var buttonWidth: CGFloat = 0.0
    var buttonHeight: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        layer.cornerRadius = 20.0
        titleLabel?.font = .promptRegular(size: 18.0)
    }
    
    func setButtonActive() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = .init(x: 0.0, y: 0.0, width: buttonWidth, height: buttonHeight)
        gradientLayer.colors = [UIColor.backgroundBlue.cgColor, UIColor.backgroundBlueV2.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.cornerRadius = 16.0
        layer.insertSublayer(gradientLayer, at: 0)
        
        setTitleColor(.textWhite, for: .normal)
        tintColor = .textWhite
    }
    
    func setButtonInActive() {
        layer.sublayers?.removeAll(where: { subLayer in
            return subLayer is CAGradientLayer
        })
        setTitleColor(.textGray, for: .normal)
        tintColor = .textGray
    }
}
