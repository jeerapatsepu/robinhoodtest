//
//  PinNumberButton.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

class PinNumberButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        layer.cornerRadius = 41.0
        titleLabel?.font = .promptMedium(size: 32.0)
        setTitleColor(.textBrown, for: .normal)
        backgroundColor = .textGray
    }
}
