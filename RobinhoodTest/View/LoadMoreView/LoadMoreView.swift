//
//  LoadMoreView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

class LoadMoreView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var isAnimating: Bool {
        return activityIndicatorView.isAnimating
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupActivityIndicatorView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        setupActivityIndicatorView()
    }
    
    private func setupView() {
        Bundle(for: LoadMoreView.self).loadNibNamed("LoadMoreView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    private func setupActivityIndicatorView() {
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.stopAnimating()
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        activityIndicatorView.stopAnimating()
    }
}
