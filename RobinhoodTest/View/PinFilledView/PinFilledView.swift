//
//  PinFilledView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

class PinFilledView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var itemViewList: [PinFilledItemView]!
    
    private let maxDigit = 6
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    private func setupView() {
        Bundle(for: PinFilledView.self).loadNibNamed("PinFilledView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setResetStyle() {
        itemViewList.forEach { pinFilledItemView in
            pinFilledItemView.setInActiveStyle()
        }
    }

    func configView(text: String) {
        guard text.count <= maxDigit else { return }
        setResetStyle()
        var index = 0
        while index < text.count {
            if itemViewList.indices.contains(index) {
                itemViewList[index].setActiveStyle()
            }
            index += 1
        }
    }
}
