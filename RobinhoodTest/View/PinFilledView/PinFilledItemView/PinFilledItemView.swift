//
//  PinFilledItemView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

class PinFilledItemView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
        setInActiveStyle()
    }
    
    private func setupView() {
        layer.cornerRadius = 12.0
    }
    
    func setInActiveStyle() {
        backgroundColor = .textGray
    }
    
    func setActiveStyle() {
        backgroundColor = .textBrown
    }
}
