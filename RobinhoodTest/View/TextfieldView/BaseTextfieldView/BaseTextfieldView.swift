//
//  BaseTextfieldView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

class BaseTextfieldView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textfieldView: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupTextfieldView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        setupTextfieldView()
    }
    
    private func setupView() {
        Bundle(for: BaseTextfieldView.self).loadNibNamed("BaseTextfieldView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        contentView.layer.cornerRadius = 20.0
        contentView.layer.borderColor = UIColor.textGray.cgColor
        contentView.layer.borderWidth = 1.0
    }

    private func setupTextfieldView() {
        textfieldView.font = .promptRegular(size: 16.0)
    }
    
    func setPlaceHolder(text: String) {
        textfieldView.placeholder = text
    }
}
