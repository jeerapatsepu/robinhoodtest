//
//  UserAuthentication.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import KeychainSwift
import UIKit

final class UserAuthentication {
    private var keychain: KeychainSwift
    static let shared = UserAuthentication()
    
    private init() {
        keychain = KeychainSwift()
    }
    
    func getUserProfilePin() -> String? {
        return keychain.get(KeychainKey.userProfilePin.rawValue)
    }
    
    func setUserProfilePin(_ userProfilePin: String) {
        keychain.set(userProfilePin, forKey: KeychainKey.userProfilePin.rawValue)
    }
    
    func getUserProfileImageUrl() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKey.userProfileImage.rawValue)
    }
    
    func getUserProfileName() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKey.userProfileName.rawValue)
    }
    
    func setAppInLastForegroundTimeStamp(timestamp: String?) {
        UserDefaults.standard.setValue(timestamp, forKey: UserDefaultsKey.appInLastForegroundTimeStamp.rawValue)
    }
    
    func getAppInLastForegroundTimeStamp() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKey.appInLastForegroundTimeStamp.rawValue)
    }
}
