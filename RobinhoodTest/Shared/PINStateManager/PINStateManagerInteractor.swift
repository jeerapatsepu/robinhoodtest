//
//  PINStateManagerInteractor.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import Foundation
import UIKit

protocol PINStateManagerInteractorBusinessLogic {
    func handleUserLeaveTimeStamp(userLeaveTimeStamp: String, appInActiveLimitInSecond: Int)
}

protocol PINStateManagerInteractorDataStore {
}

final class PINStateManagerInteractor: PINStateManagerInteractorBusinessLogic, PINStateManagerInteractorDataStore {
    var presenter: PINStateManagerPresenterInterface?
 
    func handleUserLeaveTimeStamp(userLeaveTimeStamp: String, appInActiveLimitInSecond: Int) {
        guard let userLeaveTimeStamp = Double(userLeaveTimeStamp) else { return }
        let userLeaveDate = Date(timeIntervalSince1970: userLeaveTimeStamp)
        let nowDate = Date()
        let compareDateInSecond = nowDate.timeIntervalSince(userLeaveDate)
        print("compareDateInSecond ", compareDateInSecond)
        if compareDateInSecond > Double(appInActiveLimitInSecond) {
            presenter?.presentPinViewController()
        } else {
            presenter?.presentTimeRemainingToShowPinViewController(remaining: compareDateInSecond)
        }
    }
}

