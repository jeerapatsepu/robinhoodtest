//
//  PINManager.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 4/11/2566 BE.
//

import Foundation

protocol PINStateManagerInterface: NSObjectProtocol {
    func displayPinViewController()
    func displayTimeRemainingToShowPinViewController(remaining: Int)
}

final class PINStateManager: NSObject {
    static let shared = PINStateManager()
    
    private var timer: Timer?
    private let appInActiveLimitInSecond = 10
    private var walker = 0
    
    var interactor: PINStateManagerInteractorBusinessLogic?
    
    override init() {
        super.init()
        
        let config = PINStateManagerConfigurator()
        config.configure(pinStateManager: self)
    }
    
    func handleUserLeaveTimeStamp(userLeaveTimeStamp: String) {
        print("handleUserLeaveTimeStamp")
        interactor?.handleUserLeaveTimeStamp(userLeaveTimeStamp: userLeaveTimeStamp, appInActiveLimitInSecond: appInActiveLimitInSecond)
    }
    
    func startTimer() {
        stopTimer()
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.performInactive), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        walker = 0
        timer?.invalidate()
        timer = nil
    }
    
    @objc private func performInactive() {
        if walker < appInActiveLimitInSecond {
            walker += 1
            print(walker)
        } else {
            stopTimer()
            NotificationCenter.default.post(name: NSNotification.Name(NotificationNameKey.userInactive.rawValue), object: nil)
        }
    }
}

// MARK: - PINStateManagerInterface
extension PINStateManager: PINStateManagerInterface {
    func displayPinViewController() {
        stopTimer()
        NotificationCenter.default.post(name: NSNotification.Name(NotificationNameKey.userInactive.rawValue), object: nil)
    }
    
    func displayTimeRemainingToShowPinViewController(remaining: Int) {
        stopTimer()
        walker = remaining
        
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.performInactive), userInfo: nil, repeats: true)
    }
}
