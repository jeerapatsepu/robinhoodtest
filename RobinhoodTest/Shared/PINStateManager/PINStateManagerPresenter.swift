//
//  PINStateManagerPresenter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

protocol PINStateManagerPresenterInterface {
    func presentPinViewController()
    func presentTimeRemainingToShowPinViewController(remaining: Double)
}

class PINStateManagerPresenter: PINStateManagerPresenterInterface {
    weak var pinStateManager: PINStateManagerInterface?
    
    func presentPinViewController() {
        pinStateManager?.displayPinViewController()
    }
    
    func presentTimeRemainingToShowPinViewController(remaining: Double) {
        pinStateManager?.displayTimeRemainingToShowPinViewController(remaining: Int(ceil(remaining)))
    }
}

