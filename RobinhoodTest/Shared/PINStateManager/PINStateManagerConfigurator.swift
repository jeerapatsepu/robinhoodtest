//
//  PINStateManagerConfigurator.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

final class PINStateManagerConfigurator {
    func configure(pinStateManager: PINStateManager) {
        let presenter = PINStateManagerPresenter()
        presenter.pinStateManager = pinStateManager

        let interactor = PINStateManagerInteractor()
        interactor.presenter = presenter

        pinStateManager.interactor = interactor
    }
}
