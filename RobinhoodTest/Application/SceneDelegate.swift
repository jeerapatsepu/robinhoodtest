//
//  SceneDelegate.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 30/10/2566 BE.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    private var isDidBecomeActiveFirst = true

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = BaseWindow(windowScene: windowScene)
        let controller = UIStoryboard(name: "Main", bundle: Bundle(for: MainViewController.self)).instantiateViewController(identifier: "MainView")
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        print("sceneDidBecomeActive")
        if let appInLastForegroundTimeStamp = UserAuthentication.shared.getAppInLastForegroundTimeStamp(),
           !isDidBecomeActiveFirst {
            let topViewController = BaseTool.getTopViewController()
            switch topViewController {
                case is MainViewController:
                    guard let mainViewController = topViewController as? MainViewController, mainViewController.currentPageIndex != 0 else { return }
                    PINStateManager.shared.handleUserLeaveTimeStamp(userLeaveTimeStamp: appInLastForegroundTimeStamp)
                    UserAuthentication.shared.setAppInLastForegroundTimeStamp(timestamp: nil)
                    
                case is PinViewController:
                    break
                    
                default:
                    PINStateManager.shared.handleUserLeaveTimeStamp(userLeaveTimeStamp: appInLastForegroundTimeStamp)
                    UserAuthentication.shared.setAppInLastForegroundTimeStamp(timestamp: nil)
            }
        } else {
            isDidBecomeActiveFirst = false
        }
    }

    func sceneWillResignActive(_ scene: UIScene) {
        print("sceneWillResignActive")
        PINStateManager.shared.stopTimer()
        UserAuthentication.shared.setAppInLastForegroundTimeStamp(timestamp: "\(Date().timeIntervalSince1970)")
    }
}

