//
//  MainConfigurator.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

final class MainConfigurator {
    func configure(viewController: MainViewController) {
        let router = MainRouter()
        router.viewController = viewController

        let presenter = MainPresenter()
        presenter.viewController = viewController

        let interactor = MainInteractor()
        interactor.presenter = presenter

        viewController.interactor = interactor
        viewController.router = router
    }
}
