//
//  MainWorker.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol MainWorkerInterface {
    func setUserProfilePin(_ userProfilePin: String)
}

final class MainWorker: MainWorkerInterface {

    var service: MainService?
    
    init(with service: MainService) {
        self.service = service
    }
    
    func setUserProfilePin(_ userProfilePin: String) {
        service?.setUserProfilePin(userProfilePin)
    }
}
