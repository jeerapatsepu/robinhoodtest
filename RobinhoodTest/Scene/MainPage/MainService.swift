//
//  MainService.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

class MainService {
    func setUserProfilePin(_ userProfilePin: String) {
        UserAuthentication.shared.setUserProfilePin(userProfilePin.hashWithSHA256())
    }
}
