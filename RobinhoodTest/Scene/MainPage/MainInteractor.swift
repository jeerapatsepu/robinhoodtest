//
//  MainInteractor.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol MainInteractorBusinessLogic {
    func fetchApplicationConfig()
}

protocol MainInteractorDataStore {
}

final class MainInteractor: MainInteractorBusinessLogic, MainInteractorDataStore {
    var presenter: MainPresenterInterface?
    var worker: MainWorkerInterface? = MainWorker(with: MainService())
    
    func fetchApplicationConfig() {
        worker?.setUserProfilePin("123456")
    }
}

