//
//  MainViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol MainView: BaseView {
}

class MainViewController: BaseViewController {
    @IBOutlet weak var contentView: UIView!
    
    private var mainPageViewController: MainPageViewController!
    
    var currentPageIndex = 0
    
    var interactor: MainInteractorBusinessLogic?
    var router: MainRouterInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let config = MainConfigurator()
        config.configure(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupContentView()
        interactor?.fetchApplicationConfig()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didValidateUserPinSuccess), name: NSNotification.Name(NotificationNameKey.validateUserPinSuccess.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didUserInactive), name: NSNotification.Name(NotificationNameKey.userInactive.rawValue), object: nil)
    }
    
    private func setupContentView() {
        mainPageViewController = MainPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        addChild(mainPageViewController)
        contentView.addSubview(mainPageViewController.view)
        mainPageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainPageViewController.view.topAnchor.constraint(equalTo: view.topAnchor),
            mainPageViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainPageViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mainPageViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        mainPageViewController.didMove(toParent: self)
        mainPageViewController.pagerDelegate = self
    }
    
    @objc private func didValidateUserPinSuccess() {
        mainPageViewController?.slideToPageIndex(index: 1)
    }
    
    @objc private func didUserInactive() {
        let window = BaseTool.getWindow()
        let topViewController = BaseTool.getTopViewController()
        if topViewController is MainViewController {
            window?.rootViewController?.dismiss(animated: false, completion: { [weak self] in
                self?.mainPageViewController?.slideToPageIndex(index: 0)
            })
        } else {
            router?.navigateToPinViewController()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NotificationNameKey.validateUserPinSuccess.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(NotificationNameKey.userInactive.rawValue), object: nil)
    }
}

// MARK: - MainPageViewControllerDelegate
extension MainViewController: MainPageViewControllerDelegate {
    func didPageChanged(currentPageIndex: Int) {
        self.currentPageIndex = currentPageIndex
    }
}

// MARK: - MainViewController
extension MainViewController: MainView {
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
}

