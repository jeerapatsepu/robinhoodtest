//
//  MainPageViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol MainPageViewControllerDelegate: NSObjectProtocol {
    func didPageChanged(currentPageIndex: Int)
}

class MainPageViewController: UIPageViewController {
    enum PageType {
        case pinPage
        case taskManagementPage
    }
    
    private lazy var pages: [UIViewController] = [
        getViewController(pageType: .pinPage),
        getViewController(pageType: .taskManagementPage)
    ]
    private var currentPageIndex = 0
    weak var pagerDelegate: MainPageViewControllerDelegate?
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
        
        setViewControllers([pages[currentPageIndex]], direction: .forward, animated: false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getViewController(pageType: PageType) -> UIViewController {
        switch pageType {
            case .pinPage:
                let controller = UIStoryboard(name: "Pin", bundle: Bundle(for: PinViewController.self)).instantiateViewController(identifier: "PinView") as! PinViewController
                return BaseNavigationController(rootViewController: controller)
                
            case .taskManagementPage:
                let controller = UIStoryboard(name: "TaskManagement", bundle: Bundle(for: TaskManagementViewController.self)).instantiateViewController(identifier: "TaskManagementView")
                return BaseNavigationController(rootViewController: controller)
        }
    }
    
    func slideToPageIndex(index: Int) {
        guard index != currentPageIndex else { return }
        if index > currentPageIndex {
            setViewControllers([pages[index]], direction: .forward, animated: false)
        } else  {
            setViewControllers([pages[index]], direction: .reverse, animated: false)
        }
        currentPageIndex = index
        pagerDelegate?.didPageChanged(currentPageIndex: currentPageIndex)
    }
}
