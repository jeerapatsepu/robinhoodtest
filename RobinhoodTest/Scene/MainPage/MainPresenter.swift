//
//  MainPresenter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol MainPresenterInterface {
    func startLoading()
    func stopLoading()
}

class MainPresenter: MainPresenterInterface {
    weak var viewController: MainView?
    
    func startLoading() {
        self.viewController?.startLoading()
    }
    
    func stopLoading() {
        self.viewController?.stopLoading()
    }
}

