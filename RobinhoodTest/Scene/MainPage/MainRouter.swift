//
//  MainRouter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol MainRouterInterface {
    func navigateToPinViewController()
}

final class MainRouter: MainRouterInterface {
    weak var viewController: MainViewController?

    func navigateToPinViewController() {
        let topViewController = BaseTool.getTopViewController()
        let controller = UIStoryboard(name: "Pin", bundle: Bundle(for: PinViewController.self)).instantiateViewController(identifier: "PinView") as! PinViewController
        controller.isOpenAsModalPage = true
        let baseNavigationController = BaseNavigationController(rootViewController: controller)
        topViewController?.presentFull(controller: baseNavigationController)
    }
}
