//
//  TaskDetailViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import UIKit

protocol TaskDetailView: BaseView {
}

class TaskDetailViewController: BaseViewController {
    @IBOutlet weak var iconBackground: UIView!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var taskTitle = ""
    var taskDescription = ""
    var taskIconBackgroundColor: UIColor = .white
    
    var interactor: TaskDetailInteractorBusinessLogic?
    var router: TaskDetailRouterInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let config = TaskDetailConfigurator()
        config.configure(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupIconBackground()
        setupIconLabel() 
        setupTitleLabel()
        setupDescriptionLabel()
    }
    
    private func setupView() {
        title = taskTitle
    }
    
    private func setupIconBackground() {
        iconBackground.layer.cornerRadius = 64.0
        iconBackground.backgroundColor = taskIconBackgroundColor
    }
    
    private func setupIconLabel() {
        iconLabel.setupLabel(text: taskTitle.first?.description.uppercased() ?? "", font: .promptBold(size: 64.0), color: .textWhite)
    }
    
    private func setupTitleLabel() {
        titleLabel.setupLabel(text: taskTitle, font: .promptMedium(size: 20.0), color: .textBlack)
    }
    
    private func setupDescriptionLabel() {
        descriptionLabel.setupLabel(text: taskDescription, font: .promptMedium(size: 16.0), color: .textBlack, lines: 0)
    }
}

// MARK: - TaskDetailViewController
extension TaskDetailViewController: TaskDetailView {
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
}

