//
//  TaskDetailInteractor.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import UIKit

protocol TaskDetailInteractorBusinessLogic {
}

protocol TaskDetailInteractorDataStore {
}

final class TaskDetailInteractor: TaskDetailInteractorBusinessLogic, TaskDetailInteractorDataStore {
    var presenter: TaskDetailPresenterInterface?
    var worker: TaskDetailWorkerInterface? = TaskDetailWorker(with: TaskDetailService())
    
}

