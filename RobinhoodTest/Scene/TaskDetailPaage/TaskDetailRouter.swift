//
//  TaskDetailRouter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import UIKit

protocol TaskDetailRouterInterface {
}

final class TaskDetailRouter: TaskDetailRouterInterface {
    weak var viewController: TaskDetailViewController?

}
