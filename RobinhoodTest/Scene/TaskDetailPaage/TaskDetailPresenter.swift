//
//  TaskDetailPresenter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import UIKit

protocol TaskDetailPresenterInterface {
    func startLoading()
    func stopLoading()
}

class TaskDetailPresenter: TaskDetailPresenterInterface {
    weak var viewController: TaskDetailView?
    
    func startLoading() {
        self.viewController?.startLoading()
    }
    
    func stopLoading() {
        self.viewController?.stopLoading()
    }
}

