//
//  TaskDetailWorker.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import UIKit

protocol TaskDetailWorkerInterface {
}

final class TaskDetailWorker: TaskDetailWorkerInterface {

    var service: TaskDetailService?
    
    init(with service: TaskDetailService) {
        self.service = service
    }

}
