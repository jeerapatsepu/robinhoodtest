//
//  TaskDetailConfigurator.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 8/11/2566 BE.
//

import UIKit

final class TaskDetailConfigurator {
    func configure(viewController: TaskDetailViewController) {
        let router = TaskDetailRouter()
        router.viewController = viewController

        let presenter = TaskDetailPresenter()
        presenter.viewController = viewController

        let interactor = TaskDetailInteractor()
        interactor.presenter = presenter

        viewController.interactor = interactor
        viewController.router = router
    }
}
