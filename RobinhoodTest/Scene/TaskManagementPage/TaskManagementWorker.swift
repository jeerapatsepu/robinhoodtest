//
//  TaskManagementWorker.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import SwiftyJSON
import UIKit

protocol TaskManagementWorkerInterface {
    func fetchTaskList(
        request: TaskManagementModels.Request.TaskListRequest,
        successs: @escaping (TaskManagementModels.Response.TaskListResponse?) -> Void,
        failure: @escaping (String) -> Void
    )
}

final class TaskManagementWorker: TaskManagementWorkerInterface {

    var service: TaskManagementService?
    
    init(with service: TaskManagementService) {
        self.service = service
    }

    func fetchTaskList(
        request: TaskManagementModels.Request.TaskListRequest,
        successs: @escaping (TaskManagementModels.Response.TaskListResponse?) -> Void,
        failure: @escaping (String) -> Void
    ) {
        service?.fetchTaskList(
            params: getTaskListRequestParams(request: request),
            success: { [weak self] data in
                successs(self?.mapTaskListResponseData(data: data))
            },
            failure: { error in
                failure(error.localizedDescription)
            }
        )
    }
}

// MARK: - TaskListResponse Mapper
extension TaskManagementWorker {
    private func getTaskListRequestParams(request: TaskManagementModels.Request.TaskListRequest) -> [String : String] {
        let params: [String: String] = [
            "offset" : "\(request.offset)",
            "limit" : "\(request.limit)",
            "isAsc" : "\(request.isAsc)",
            "status" : request.status.rawValue,
        ]
        return params
    }
    
    private func mapTaskListResponseData(data: Data) -> TaskManagementModels.Response.TaskListResponse? {
        do {
            let dataJson = try JSON(data: data)
            return TaskManagementModels.Response.TaskListResponse(
                tasks: getTaskList(data: dataJson),
                pageNumber: dataJson["pageNumber"].intValue,
                totalPages: dataJson["totalPages"].intValue
            )
        } catch {
            return nil
        }
    }
    
    private func getTaskList(data: JSON) -> [TaskManagementModels.Response.TaskItem] {
        return data["tasks"].arrayValue.map { jsonData in
            return TaskManagementModels.Response.TaskItem(
                id: jsonData["id"].stringValue,
                title: jsonData["title"].stringValue,
                description: jsonData["description"].stringValue,
                createdAt: jsonData["createdAt"].stringValue,
                status: jsonData["status"].stringValue
            )
        }
    }
}
