//
//  TaskManagementModels.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

struct TaskManagementModels {
    struct Request {
        struct TaskListRequest {
            let offset: Int
            let limit: Int
            let sortBy: TaskManagementModels.ViewModel.TaskSortByType
            let isAsc: Bool
            let status : TaskManagementModels.ViewModel.TaskStatusType
        }
    }
    
    struct Response {
        struct TaskListResponse {
            let tasks: [TaskItem]
            let pageNumber: Int
            let totalPages: Int
        }
        
        struct TaskItem {
            let id: String
            let title: String
            let description: String
            let createdAt: String
            let status: String
        }
    }
    
    struct ViewModel {
        enum TaskStatusType: String {
            case toDo = "TODO"
            case doing = "DOING"
            case done = "DONE"
        }
        
        enum TaskSortByType: String {
            case ascending = "ascending"
            case descending = "descending"
        }
        
        struct TaskListViewModel {
            let dateText: String
            var taskList: [TaskItem]
        }
        
        struct TaskItem {
            let id: String
            let iconText: String
            let iconBackground: UIColor
            let title: String
            let description: String
            let date: String
        }
    }
}
