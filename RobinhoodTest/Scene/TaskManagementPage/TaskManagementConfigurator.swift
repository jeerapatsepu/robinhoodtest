//
//  TaskManagementConfigurator.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

final class TaskManagementConfigurator {
    func configure(viewController: TaskManagementViewController) {
        let router = TaskManagementRouter()
        router.viewController = viewController

        let presenter = TaskManagementPresenter()
        presenter.viewController = viewController

        let interactor = TaskManagementInteractor()
        interactor.presenter = presenter

        viewController.interactor = interactor
        viewController.router = router
    }
}
