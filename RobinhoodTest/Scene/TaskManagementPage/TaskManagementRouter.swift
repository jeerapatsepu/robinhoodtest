//
//  TaskManagementRouter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

protocol TaskManagementRouterInterface {
    func navigateToCreateTaskViewController()
    func navigateToTaskDetailViewController(title: String, description: String, iconBackgroundColor: UIColor)
}

final class TaskManagementRouter: TaskManagementRouterInterface {
    weak var viewController: TaskManagementViewController?

    func navigateToCreateTaskViewController() {
        let controller = UIStoryboard(name: "CreateTask", bundle: Bundle(for: CreateTaskViewController.self)).instantiateViewController(identifier: "CreateTaskView") as! CreateTaskViewController
        controller.delegate = viewController
        let baseNavigationController = BaseNavigationController(rootViewController: controller)
        viewController?.presentFull(controller: baseNavigationController)
    }
    
    func navigateToTaskDetailViewController(title: String, description: String, iconBackgroundColor: UIColor) {
        let controller = UIStoryboard(name: "TaskDetail", bundle: Bundle(for: TaskDetailViewController.self)).instantiateViewController(identifier: "TaskDetailView") as! TaskDetailViewController
        controller.taskTitle = title
        controller.taskDescription = description
        controller.taskIconBackgroundColor = iconBackgroundColor
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
