//
//  TaskManagementViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

protocol TaskManagementView: BaseView {
    func displayTaskListSuccess(display: [TaskManagementModels.ViewModel.TaskListViewModel])
    func displayTaskListFail(text: String)
    func displayRemoveItemSuccess(display: [TaskManagementModels.ViewModel.TaskListViewModel])
    func displayAddItemSuccess(display: [TaskManagementModels.ViewModel.TaskListViewModel])
    func startLoadMoreLoading()
    func stopLoadMoreLoading()
}

class TaskManagementViewController: BaseViewController {
    @IBOutlet weak var descriptionContentView: UIView!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var todoStatusHeaderView: TodoStatusHeaderView!
    @IBOutlet weak var createTaskButton: CreateTaskButton!
    
    @IBOutlet weak var taskListTableView: UITableView!
    @IBOutlet weak var loadMoreView: LoadMoreView!
    
    private var currentTaskListStatusType: TaskManagementModels.ViewModel.TaskStatusType = .toDo
    private var taskList: [TaskManagementModels.ViewModel.TaskListViewModel] = []
    private let refreshControl = UIRefreshControl()
    private var isCanLoadMore = false
    
    var interactor: TaskManagementInteractorBusinessLogic?
    var router: TaskManagementRouterInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let config = TaskManagementConfigurator()
        config.configure(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupDescriptionContentView()
        setupCornerView()
        setupWelcomeLabel()
        setupDescriptionLabel()
        setupTodoStatusHeaderView()
        setupTaskListTableView()
        setupCreateTaskButton()
        
        interactor?.fetchTaskList(taskStatus: currentTaskListStatusType, taskList: [])
    }
    
    private func setupView() {
        setupRightProfileView()
    }
    
    private func setupDescriptionContentView() {
        descriptionContentView.backgroundColor = .baseNavigationBar
    }
    
    private func setupCornerView() {
        cornerView.backgroundColor = .baseNavigationBar
        cornerView.bounds = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 32.0)
        cornerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 16.0)
    }
    
    private func setupWelcomeLabel() {
        let userName: String = UserAuthentication.shared.getUserProfileName() ?? "User"
        welcomeLabel.setupLabel(text: "Hi! \(userName)", font: .promptBold(size: 32.0), color: .textBrown)
    }
    
    private func setupDescriptionLabel() {
        descriptionLabel.setupLabel(text: "Welcome and please complain my project at jeerapatsepu@gmail.com, Thanks", font: .promptMedium(size: 18.0), color: .textBrown, lines: 0)
    }
    
    private func setupTodoStatusHeaderView() {
        todoStatusHeaderView.setButtonActive(type: currentTaskListStatusType)
        todoStatusHeaderView.delegate = self
    }
    
    private func setupTaskListTableView() {
        taskListTableView.delegate = self
        taskListTableView.dataSource = self
        taskListTableView.separatorStyle = .none
        taskListTableView.sectionHeaderTopPadding = 0.0
        taskListTableView.register(UINib(nibName: "TaskManagementTableViewCell", bundle: Bundle(for: TaskManagementTableViewCell.self)), forCellReuseIdentifier: "TaskManagementTableViewCell")
        taskListTableView.register(UINib(nibName: "TaskManagementEmptyTableViewCell", bundle: Bundle(for: TaskManagementEmptyTableViewCell.self)), forCellReuseIdentifier: "TaskManagementEmptyTableViewCell")
        
        taskListTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTaskList), for: .valueChanged)
        
        let descriptionTextHeight = (descriptionLabel.text ?? "").height(withConstrainedWidth: UIScreen.main.bounds.width - 64.0, font: .promptMedium(size: 18.0))
        taskListTableView.tableHeaderView?.frame.size.height = descriptionTextHeight + 193.0
    }
    
    private func setupCreateTaskButton() {
        createTaskButton.buttonHeight = 40
        createTaskButton.buttonWidth = 40
        createTaskButton.setTitle("+", for: .normal)
        createTaskButton.setButtonActive()
        createTaskButton.addTarget(self, action: #selector(tapCreateTaskButton), for: .touchUpInside)
    }
    
    @objc private func refreshTaskList() {
        isCanLoadMore = false
        currentTaskListStatusType = .toDo
        todoStatusHeaderView.setButtonActive(type: currentTaskListStatusType)
        interactor?.fetchTaskList(taskStatus: currentTaskListStatusType, taskList: [])
    }
    
    @objc private func tapCreateTaskButton() {
        router?.navigateToCreateTaskViewController()
    }
}

// MARK: - TodoStatusHeaderViewDelegate
extension TaskManagementViewController: TodoStatusHeaderViewDelegate {
    func didTapTodoStatusHeaderButton(type: TaskManagementModels.ViewModel.TaskStatusType) {
        guard type != currentTaskListStatusType else { return }
        currentTaskListStatusType = type
        todoStatusHeaderView.setButtonActive(type: currentTaskListStatusType)
        interactor?.fetchTaskList(taskStatus: currentTaskListStatusType, taskList: [])
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension TaskManagementViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if taskList[section].taskList.isEmpty {
            return 1
        }
        return taskList[section].taskList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let taskList = taskList[indexPath.section].taskList
        if taskList.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskManagementEmptyTableViewCell", for: indexPath) as! TaskManagementEmptyTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskManagementTableViewCell", for: indexPath) as! TaskManagementTableViewCell
            let cellData = taskList[indexPath.row]
            cell.configCell(data: cellData)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = TaskManagementHeaderView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        headerView.configView(title: taskList[section].dateText)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let isLastSectionAndRow = (indexPath.section == taskList.count - 1 && indexPath.row == taskList[indexPath.section].taskList.count - 1)
        guard isLastSectionAndRow, isCanLoadMore, !loadMoreView.isAnimating else { return }
        isCanLoadMore = false
        interactor?.fetchTaskListWithLoadMore(taskStatus: currentTaskListStatusType, taskList: taskList)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isCanLoadMore = true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let sectionData = taskList[indexPath.section]
        guard !sectionData.taskList.isEmpty else { return nil }
        let deleteAction = UIContextualAction(style: .destructive, title: "Remove") { [unowned self] saction, rowView, bool in
            interactor?.removeItem(taskList: taskList, indexPath: indexPath)
        }
        deleteAction.backgroundColor = .orange
        deleteAction.image = UIImage(systemName: "minus.circle.fill")
        let config = UISwipeActionsConfiguration(actions: [deleteAction])
        return config
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionData = taskList[indexPath.section]
        guard !sectionData.taskList.isEmpty, sectionData.taskList.indices.contains(indexPath.row) else { return }
        let data = sectionData.taskList[indexPath.row]
        router?.navigateToTaskDetailViewController(title: data.title, description: data.description, iconBackgroundColor: data.iconBackground)
    }
}

// MARK: - CreateTaskViewControllerDelegate
extension TaskManagementViewController: CreateTaskViewControllerDelegate {
    func didCreateTaskSuccess(data: TaskManagementModels.ViewModel.TaskItem) {
        print("didCreateTaskSuccess")
        interactor?.addItem(taskList: taskList, data: data)
    }
}

// MARK: - TaskManagementViewController
extension TaskManagementViewController: TaskManagementView {
    func displayTaskListSuccess(display: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        taskList = display
        taskListTableView.reloadData()
    }
    
    func displayTaskListFail(text: String) {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        present(alert, animated: true)
    }
    
    func displayRemoveItemSuccess(display: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        taskList = display
        taskListTableView.reloadData()
    }
    
    func displayAddItemSuccess(display: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        taskListTableView.setContentOffset(.zero, animated: false)
        currentTaskListStatusType = .toDo
        todoStatusHeaderView.setButtonActive(type: currentTaskListStatusType)
        taskList = display
        taskListTableView.reloadData()
    }
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    func startLoadMoreLoading() {
        loadMoreView.startLoading()
    }
    
    func stopLoadMoreLoading() {
        loadMoreView.stopLoading()
    }
}
