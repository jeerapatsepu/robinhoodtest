//
//  TaskManagementHeaderView.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 2/11/2566 BE.
//

import UIKit

class TaskManagementHeaderView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupTitleLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        setupTitleLabel()
    }
    
    private func setupView() {
        Bundle(for: TaskManagementHeaderView.self).loadNibNamed("TaskManagementHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    private func setupTitleLabel() {
        titleLabel.setupLabel(text: "", font: .promptMedium(size: 18.0), color: .textBrown)
    }
    
    func configView(title: String) {
        titleLabel.text = title
    }
}
