//
//  TaskManagementService.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import RobinhoodNetwork
import UIKit

class TaskManagementService {
    private let taskListAPI = TaskListAPI()
    
    func fetchTaskList(params: [String : String]?, success: @escaping (Data) -> Void, failure: @escaping (Error) -> Void) {
        taskListAPI.getTaskList(
            params: params,
            completion: { response in
                switch response {
                    case let .success(data):
                        success(data)
                    
                    case let .failure(error):
                        failure(error)
                }
            }
        )
    }
}
