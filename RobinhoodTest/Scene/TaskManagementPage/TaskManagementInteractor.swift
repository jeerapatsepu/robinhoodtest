//
//  TaskManagementInteractor.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

protocol TaskManagementInteractorBusinessLogic {
    func fetchTaskList(taskStatus: TaskManagementModels.ViewModel.TaskStatusType, taskList: [TaskManagementModels.ViewModel.TaskListViewModel])
    func fetchTaskListWithLoadMore(taskStatus: TaskManagementModels.ViewModel.TaskStatusType, taskList: [TaskManagementModels.ViewModel.TaskListViewModel])
    func removeItem(taskList: [TaskManagementModels.ViewModel.TaskListViewModel], indexPath: IndexPath)
    func addItem(
        taskList: [TaskManagementModels.ViewModel.TaskListViewModel],
        data: TaskManagementModels.ViewModel.TaskItem
    )
}

protocol TaskManagementInteractorDataStore {
}

final class TaskManagementInteractor: TaskManagementInteractorBusinessLogic, TaskManagementInteractorDataStore {
    var presenter: TaskManagementPresenterInterface?
    var worker: TaskManagementWorkerInterface? = TaskManagementWorker(with: TaskManagementService())
    
    private var isEndData = false
    private var offset = 0
    private let limit = 10
    
    func fetchTaskList(taskStatus: TaskManagementModels.ViewModel.TaskStatusType, taskList: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        offset = 0
        
        let request = getTaskListRequest(offset: offset, limit: limit, taskStatus: taskStatus)
        presenter?.startLoading()
        worker?.fetchTaskList(
            request: request,
            successs: { [unowned self] response in
                self.presenter?.stopLoading()
                if let response = response, !response.tasks.isEmpty {
                    self.presenter?.presentTaskListSuccess(response: response.tasks, taskList: taskList)
                    self.isEndData = response.tasks.count < limit
                    self.offset += 1
                } else {
                    self.isEndData = true
                    self.presenter?.presentTaskListFail(text: "Please try again")
                }
            },
            failure: { [weak self] _ in
                self?.presenter?.stopLoading()
                self?.presenter?.presentTaskListFail(text: "Please try again")
            }
        )
    }
    
    func fetchTaskListWithLoadMore(taskStatus: TaskManagementModels.ViewModel.TaskStatusType, taskList: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        if !isEndData {
            let request = getTaskListRequest(offset: offset, limit: limit, taskStatus: taskStatus)
            presenter?.startLoadMoreLoading()
            worker?.fetchTaskList(
                request: request,
                successs: { [unowned self] response in
                    self.presenter?.stopLoadMoreLoading()
                    if let response = response, !response.tasks.isEmpty {
                        self.presenter?.presentTaskListWithLoadMoreSuccess(response: response.tasks, taskList: taskList)
                        self.isEndData = response.tasks.count < limit
                        self.offset += 1
                    } else {
                        self.isEndData = true
                    }
                },
                failure: { [weak self] _ in
                    self?.presenter?.stopLoadMoreLoading()
                }
            )
        }
    }
    
    func removeItem(taskList: [TaskManagementModels.ViewModel.TaskListViewModel], indexPath: IndexPath) {
        presenter?.presentRemoveItemSuccess(taskList: taskList, indexPath: indexPath)
    }
    
    func addItem(
        taskList: [TaskManagementModels.ViewModel.TaskListViewModel],
        data: TaskManagementModels.ViewModel.TaskItem
    ) {
        presenter?.presentAddItemSuccess(taskList: taskList, data: data)
    }
    
    private func getTaskListRequest(offset: Int, limit: Int, taskStatus: TaskManagementModels.ViewModel.TaskStatusType) -> TaskManagementModels.Request.TaskListRequest {
        return TaskManagementModels.Request.TaskListRequest(
            offset: offset,
            limit: limit,
            sortBy: .descending,
            isAsc: true,
            status: taskStatus
        )
    }
}
