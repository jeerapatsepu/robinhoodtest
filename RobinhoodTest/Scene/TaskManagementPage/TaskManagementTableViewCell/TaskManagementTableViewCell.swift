//
//  TaskManagementTableViewCell.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 2/11/2566 BE.
//

import UIKit

class TaskManagementTableViewCell: UITableViewCell {
    @IBOutlet weak var iconBackgroundView: UIView!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
        setupIconBackgroundView()
        setupIconLabel()
        setupTitleLabel()
        setupDescriptionLabel()
    }

    private func setupView() {
        selectionStyle = .none
    }

    private func setupIconBackgroundView() {
        iconBackgroundView.layer.cornerRadius = 8.0
    }
    
    private func setupIconLabel() {
        iconLabel.setupLabel(text: "", font: .promptBold(size: 32.0), color: .textWhite)
    }
    
    private func setupTitleLabel() {
        titleLabel.setupLabel(text: "", font: .promptMedium(size: 16.0), color: .textBlack)
    }
    
    private func setupDescriptionLabel() {
        descriptionLabel.setupLabel(text: "", font: .promptMedium(size: 14.0), color: .textGray, lines: 2)
    }
    
    func configCell(data: TaskManagementModels.ViewModel.TaskItem) {
        titleLabel.text = data.title
        descriptionLabel.text = data.description
        iconLabel.text = data.iconText
        iconBackgroundView.backgroundColor = data.iconBackground
    }
}
