//
//  TaskManagementPresenter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import UIKit

protocol TaskManagementPresenterInterface {
    func presentTaskListSuccess(response: [TaskManagementModels.Response.TaskItem], taskList: [TaskManagementModels.ViewModel.TaskListViewModel])
    func presentTaskListWithLoadMoreSuccess(response: [TaskManagementModels.Response.TaskItem], taskList: [TaskManagementModels.ViewModel.TaskListViewModel])
    func presentTaskListFail(text: String)
    func presentRemoveItemSuccess(taskList: [TaskManagementModels.ViewModel.TaskListViewModel], indexPath: IndexPath)
    func presentAddItemSuccess(taskList: [TaskManagementModels.ViewModel.TaskListViewModel], data: TaskManagementModels.ViewModel.TaskItem)
    func startLoading()
    func stopLoading()
    func startLoadMoreLoading()
    func stopLoadMoreLoading()
}

class TaskManagementPresenter: TaskManagementPresenterInterface {
    weak var viewController: TaskManagementView?
    
    func presentTaskListSuccess(response: [TaskManagementModels.Response.TaskItem], taskList: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        viewController?.displayTaskListSuccess(display: getTaskListViewModel(response: response, taskList: taskList))
    }
    
    func presentTaskListWithLoadMoreSuccess(response: [TaskManagementModels.Response.TaskItem], taskList: [TaskManagementModels.ViewModel.TaskListViewModel]) {
        viewController?.displayTaskListSuccess(display: getOtherDateTaskList(response: response, taskList: taskList))
    }
    
    func presentTaskListFail(text: String) {
        viewController?.displayTaskListFail(text: text)
    }
    
    func presentRemoveItemSuccess(taskList: [TaskManagementModels.ViewModel.TaskListViewModel], indexPath: IndexPath) {
        var tmpTaskList = taskList
        let isTaskListContainsItem = tmpTaskList[indexPath.section].taskList.indices.contains(indexPath.row)
        if isTaskListContainsItem {
            tmpTaskList[indexPath.section].taskList.remove(at: indexPath.row)
        }
        
        if indexPath.section > 0 {
            let isSectionEmpty = tmpTaskList[indexPath.section].taskList.isEmpty
            if isSectionEmpty {
                tmpTaskList.remove(at: indexPath.section)
            }
        }
        viewController?.displayRemoveItemSuccess(display: tmpTaskList)
    }
    
    func presentAddItemSuccess(taskList: [TaskManagementModels.ViewModel.TaskListViewModel], data: TaskManagementModels.ViewModel.TaskItem) {
        var tmpTaskList = taskList
        guard tmpTaskList.indices.contains(0) else { return }
        tmpTaskList[0].taskList.append(data)
        viewController?.displayAddItemSuccess(display: tmpTaskList)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func startLoadMoreLoading() {
        viewController?.startLoadMoreLoading()
    }
    
    func stopLoadMoreLoading() {
        viewController?.stopLoadMoreLoading()
    }
}

// MARK: - TaskList ViewModel Mapper
extension TaskManagementPresenter {
    private func getTaskListViewModel(response: [TaskManagementModels.Response.TaskItem], taskList: [TaskManagementModels.ViewModel.TaskListViewModel]) -> [TaskManagementModels.ViewModel.TaskListViewModel] {
        var viewModel: [TaskManagementModels.ViewModel.TaskListViewModel] = []
        
        viewModel.append(TaskManagementModels.ViewModel.TaskListViewModel(dateText: "Today", taskList: getTodayTaskList(response: response)))
        viewModel.append(TaskManagementModels.ViewModel.TaskListViewModel(dateText: "Tomorrow", taskList: getTodayTaskList(response: response)))
        viewModel.append(contentsOf: getOtherDateTaskList(response: response, taskList: taskList))
        
        return viewModel
    }
    
    private func getTodayTaskList(response: [TaskManagementModels.Response.TaskItem]) -> [TaskManagementModels.ViewModel.TaskItem] {
        var todayTaskResponseList: [TaskManagementModels.Response.TaskItem] = []
        todayTaskResponseList = response.filter { taskItem in
            return taskItem.createdAt.isToday
        }
        return todayTaskResponseList.map { taskItem in
            return TaskManagementModels.ViewModel.TaskItem(id: taskItem.id, iconText: taskItem.title.first?.uppercased() ?? "", iconBackground: .random, title: taskItem.title, description: taskItem.description, date: taskItem.createdAt)
        }
    }
    
    private func getTomorrowTaskList(response: [TaskManagementModels.Response.TaskItem]) -> [TaskManagementModels.ViewModel.TaskItem] {
        var tomorrowTaskResponseList: [TaskManagementModels.Response.TaskItem] = []
        tomorrowTaskResponseList = response.filter { taskItem in
            return taskItem.createdAt.isTomorrow
        }
        return tomorrowTaskResponseList.map { taskItem in
            return TaskManagementModels.ViewModel.TaskItem(id: taskItem.id, iconText: taskItem.title.first?.uppercased() ?? "", iconBackground: .random, title: taskItem.title, description: taskItem.description, date: taskItem.createdAt)
        }
    }
    
    private func getOtherDateTaskList(response: [TaskManagementModels.Response.TaskItem], taskList: [TaskManagementModels.ViewModel.TaskListViewModel]) -> [TaskManagementModels.ViewModel.TaskListViewModel] {
        var dateTaskResponseList = response
        dateTaskResponseList.removeAll { taskItem in
            let shoudRemove = taskItem.createdAt.isToday || taskItem.createdAt.isTomorrow
            return shoudRemove
        }
        
        var viewModel = taskList
        dateTaskResponseList.forEach { taskItem in
            let dateText = taskItem.createdAt.date.dateTitle.uppercased()
            if !viewModel.contains(where: { viewModel in
                return viewModel.dateText == dateText
            }) {
                viewModel.append(TaskManagementModels.ViewModel.TaskListViewModel(
                    dateText: dateText,
                    taskList: [TaskManagementModels.ViewModel.TaskItem(id: taskItem.id, iconText: taskItem.title.first?.uppercased() ?? "", iconBackground: .random, title: taskItem.title, description: taskItem.description, date: taskItem.createdAt)]
                ))
            } else {
                if let sameDateSectionIndex = viewModel.firstIndex(where: { taskListViewModel in
                    return dateText == taskListViewModel.dateText
                }) {
                    guard viewModel.indices.contains(sameDateSectionIndex) else { return }
                    viewModel[sameDateSectionIndex].taskList.append(TaskManagementModels.ViewModel.TaskItem(id: taskItem.id, iconText: taskItem.title.first?.uppercased() ?? "", iconBackground: .random, title: taskItem.title, description: taskItem.description, date: taskItem.createdAt))
                }
            }
        }
        
        return viewModel
    }
}
