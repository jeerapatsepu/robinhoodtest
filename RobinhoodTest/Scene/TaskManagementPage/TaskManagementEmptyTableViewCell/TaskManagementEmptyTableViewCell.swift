//
//  TaskManagementEmptyTableViewCell.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

class TaskManagementEmptyTableViewCell: UITableViewCell {
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
        setupCornerView()
        setupEmptyLabel()
    }

    private func setupView() {
        selectionStyle = .none
    }
    
    private func setupCornerView() {
        cornerView.layer.cornerRadius = 16.0
        cornerView.backgroundColor = .backgroundGray
    }

    private func setupEmptyLabel() {
        emptyLabel.setupLabel(text: "EMPTY", font: .promptRegular(size: 14.0), color: .textGray)
    }
}
