//
//  CreateTaskViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

protocol CreateTaskViewControllerDelegate: NSObjectProtocol {
    func didCreateTaskSuccess(data: TaskManagementModels.ViewModel.TaskItem)
}

protocol CreateTaskView: BaseView {
    func displayCreateTaskSuccess(display: TaskManagementModels.ViewModel.TaskItem)
}

class CreateTaskViewController: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTextfieldView: BaseTextfieldView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextfieldView: BaseTextfieldView!
    
    weak var delegate: CreateTaskViewControllerDelegate?
    
    var interactor: CreateTaskInteractorBusinessLogic?
    var router: CreateTaskRouterInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let config = CreateTaskConfigurator()
        config.configure(viewController: self)
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupTitleLabel()
        setupDescriptionLabel()
    }
    
    private func setupView() {
        title = "Create a task"
        setupTapViewToCancelKeyboard()
        
        let createButton = UIButton(type: .system)
        createButton.setTitle("Create", for: .normal)
        createButton.titleLabel?.font = .promptRegular(size: 14.0)
        createButton.setTitleColor(.textBlack, for: .normal)
        createButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            createButton.heightAnchor.constraint(equalToConstant: 32.0)
        ])
        createButton.addTarget(self, action: #selector(tapCreateButton), for: .touchUpInside)
        
        let cancelButton = UIButton(type: .system)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.titleLabel?.font = .promptRegular(size: 14.0)
        cancelButton.setTitleColor(.textBlack, for: .normal)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cancelButton.heightAnchor.constraint(equalToConstant: 32.0)
        ])
        cancelButton.addTarget(self, action: #selector(tapCancelButton), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: createButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: cancelButton)
    }
    
    private func setupTitleLabel() {
        titleLabel.setupLabel(text: "Title", font: .promptMedium(size: 16.0), color: .textBlack)
    }
    
    private func setupDescriptionLabel() {
        descriptionLabel.setupLabel(text: "Description", font: .promptMedium(size: 16.0), color: .textBlack)
    }
    
    @objc private func tapCreateButton() {
        interactor?.createTask(title: titleTextfieldView.textfieldView.text ?? "", description: descriptionTextfieldView.textfieldView.text ?? "")
    }
    
    @objc private func tapCancelButton() {
        router?.navigateToRootViewController()
    }
}

// MARK: - CreateTaskViewController
extension CreateTaskViewController: CreateTaskView {
    func displayCreateTaskSuccess(display: TaskManagementModels.ViewModel.TaskItem) {
        delegate?.didCreateTaskSuccess(data: display)
        router?.navigateToRootViewController()
    }
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
}

