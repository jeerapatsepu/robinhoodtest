//
//  CreateTaskPresenter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

protocol CreateTaskPresenterInterface {
    func presentCreateTaskSuccess(title: String, description: String)
    func startLoading()
    func stopLoading()
}

class CreateTaskPresenter: CreateTaskPresenterInterface {
    weak var viewController: CreateTaskView?
   
    func presentCreateTaskSuccess(title: String, description: String) {
        let viewModel = TaskManagementModels.ViewModel.TaskItem(
            id: BaseTool.randomString(length: 24),
            iconText: title.first?.description.uppercased() ?? "",
            iconBackground: .random,
            title: title,
            description: description,
            date: Date().getAPIDateFormatString()
        )
        viewController?.displayCreateTaskSuccess(display: viewModel)
    }
    
    func startLoading() {
        self.viewController?.startLoading()
    }
    
    func stopLoading() {
        self.viewController?.stopLoading()
    }
}

