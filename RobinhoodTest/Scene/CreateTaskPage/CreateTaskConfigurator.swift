//
//  CreateTaskConfigurator.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

final class CreateTaskConfigurator {
    func configure(viewController: CreateTaskViewController) {
        let router = CreateTaskRouter()
        router.viewController = viewController

        let presenter = CreateTaskPresenter()
        presenter.viewController = viewController

        let interactor = CreateTaskInteractor()
        interactor.presenter = presenter

        viewController.interactor = interactor
        viewController.router = router
    }
}
