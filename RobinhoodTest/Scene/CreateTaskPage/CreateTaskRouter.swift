//
//  CreateTaskRouter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

protocol CreateTaskRouterInterface {
    func navigateToRootViewController()
}

final class CreateTaskRouter: CreateTaskRouterInterface {
    weak var viewController: CreateTaskViewController?

    func navigateToRootViewController() {
        viewController?.dismiss(animated: true)
    }
}
