//
//  CreateTaskInteractor.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

protocol CreateTaskInteractorBusinessLogic {
    func createTask(title: String, description: String)
}

protocol CreateTaskInteractorDataStore {
}

final class CreateTaskInteractor: CreateTaskInteractorBusinessLogic, CreateTaskInteractorDataStore {
    var presenter: CreateTaskPresenterInterface?
    var worker: CreateTaskWorkerInterface? = CreateTaskWorker(with: CreateTaskService())
    
    func createTask(title: String, description: String) {
        guard validateTaskForm(title: title, description: description) else {
            return
        }
        presenter?.presentCreateTaskSuccess(title: title, description: description)
    }
    
    private func validateTaskForm(title: String, description: String) -> Bool {
        return (title.count > 0) && (description.count > 0)
    }
}

