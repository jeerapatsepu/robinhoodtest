//
//  CreateTaskWorker.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

protocol CreateTaskWorkerInterface {
}

final class CreateTaskWorker: CreateTaskWorkerInterface {

    var service: CreateTaskService?
    
    init(with service: CreateTaskService) {
        self.service = service
    }

}
