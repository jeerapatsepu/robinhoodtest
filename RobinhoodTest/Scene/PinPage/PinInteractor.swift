//
//  PinInteractor.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol PinInteractorBusinessLogic {
    func getPinText() -> String
    func clearPinText()
    func validatePinText(pin: String)
}

protocol PinInteractorDataStore {
}

final class PinInteractor: PinInteractorBusinessLogic, PinInteractorDataStore {
    var presenter: PinPresenterInterface?
    var worker: PinWorkerInterface? = PinWorker(with: PinService())
    
    private var pinText: String = ""
    private let maxDegit = 6
    
    func getPinText() -> String {
        return pinText
    }
    
    func clearPinText() {
        pinText = ""
    }
    
    func validatePinText(pin: String) {
        guard pinText.count < maxDegit else { return }
        pinText.append(pin)
        if let pinTextKeychain = worker?.getUserPin(), pinTextKeychain == pinText.hashWithSHA256() {
            presenter?.presentValidatePinTextSuccess()
        } else if pinText.count >= maxDegit {
            presenter?.presentValidatePinTextFail(text: "Fail!")
        }
    }
}

