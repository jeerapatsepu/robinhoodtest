//
//  PinViewController.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol PinView: BaseView {
    func displayValidatePinTextSuccess()
    func displayValidatePinTextFail(text: String)
}

class PinViewController: BaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pinFilledView: PinFilledView!
    @IBOutlet var pinNumberButtonList: [PinNumberButton]!
    
    var isOpenAsModalPage = false
    
    var interactor: PinInteractorBusinessLogic?
    var router: PinRouterInterface?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let config = PinConfigurator()
        config.configure(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupTitleLabel()
        setupPinNumberButtonList()
    }
    
    private func setupView() {
        view.backgroundColor = .baseNavigationBar
    }
    
    private func setupTitleLabel() {
        titleLabel.setupLabel(text: "Authentication with PIN", font: .promptMedium(size: 18.0), color: .textBrown, lines: 0)
        titleLabel.textAlignment = .center
    }
    
    private func setupPinNumberButtonList() {
        for pinNumberButton in pinNumberButtonList {
            pinNumberButton.addTarget(self, action: #selector(tapPinNumberButton(_:)), for: .touchUpInside)
        }
    }
    
    @objc private func tapPinNumberButton(_ sender: UIButton) {
        guard let pin = sender.titleLabel?.text else { return }
        interactor?.validatePinText(pin: pin)
        if let pinText = interactor?.getPinText(), pinText.count > 0 {
            setupTitleLabel()
            pinFilledView.configView(text: pinText)
        }
    }
}

// MARK: - PinViewController
extension PinViewController: PinView {
    func displayValidatePinTextSuccess() {
        PINStateManager.shared.startTimer()
        interactor?.clearPinText()
        pinFilledView.setResetStyle()
        if isOpenAsModalPage {
            router?.navigateToRootViewController()
        } else {
            NotificationCenter.default.post(Notification(name: Notification.Name(NotificationNameKey.validateUserPinSuccess.rawValue)))
        }
    }
    
    func displayValidatePinTextFail(text: String) {
        interactor?.clearPinText()
        pinFilledView.setResetStyle()
        titleLabel.setupLabel(text: text, font: .promptMedium(size: 18.0), color: .red, lines: 0)
    }
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
}

