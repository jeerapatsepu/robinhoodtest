//
//  PinPresenter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol PinPresenterInterface {
    func presentValidatePinTextSuccess()
    func presentValidatePinTextFail(text: String)
    func startLoading()
    func stopLoading()
}

class PinPresenter: PinPresenterInterface {
    weak var viewController: PinView?
    
    func presentValidatePinTextSuccess() {
        viewController?.displayValidatePinTextSuccess()
    }
    
    func presentValidatePinTextFail(text: String) {
        viewController?.displayValidatePinTextFail(text: text)
    }
    
    func startLoading() {
        self.viewController?.startLoading()
    }
    
    func stopLoading() {
        self.viewController?.stopLoading()
    }
}
