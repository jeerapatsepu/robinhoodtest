//
//  PinService.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

class PinService {
    func getUserPin() -> String? {
        return UserAuthentication.shared.getUserProfilePin()
    }
}
