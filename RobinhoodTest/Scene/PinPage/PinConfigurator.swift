//
//  PinConfigurator.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

final class PinConfigurator {
    func configure(viewController: PinViewController) {
        let router = PinRouter()
        router.viewController = viewController

        let presenter = PinPresenter()
        presenter.viewController = viewController

        let interactor = PinInteractor()
        interactor.presenter = presenter

        viewController.interactor = interactor
        viewController.router = router
    }
}
