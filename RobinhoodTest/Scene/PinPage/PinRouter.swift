//
//  PinRouter.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol PinRouterInterface {
    func navigateToRootViewController()
}

final class PinRouter: PinRouterInterface {
    weak var viewController: PinViewController?

    func navigateToRootViewController() {
        viewController?.dismiss(animated: true)
    }
}
