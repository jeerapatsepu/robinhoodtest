//
//  PinWorker.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 3/11/2566 BE.
//

import UIKit

protocol PinWorkerInterface {
    func getUserPin() -> String?
}

class PinWorker: PinWorkerInterface {

    var service: PinService?
    
    init(with service: PinService) {
        self.service = service
    }
    
    func getUserPin() -> String? {
        return service?.getUserPin()
    }
}
