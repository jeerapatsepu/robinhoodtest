//
//  UIViewExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import UIKit

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func roundWithBorder(
        cornerRadius: CGFloat,
        borderColor: UIColor, 
        borderWidth: CGFloat = 1.0
    ) { 
        clipsToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
    }
}
