//
//  UIColorExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import UIKit

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1), alpha: 1.0)
    }
    
    static let baseNavigationBar = UIColor(red: 234.0/255.0, green: 237.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let baseBorderColor = UIColor(red: 236.0/255.0, green: 238.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    
    static let textBlack = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
    
    static let textWhite = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    static let textBrown = UIColor(red: 118.0/255.0, green: 108.0/255.0, blue: 115.0/255.0, alpha: 1.0)
    
    static let textGray = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    static let backgroundGray = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    
    static let backgroundBlue = UIColor(red: 148.0/255.0, green: 192.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let backgroundBlueV2 = UIColor(red: 143.0/255.0, green: 164.0/255.0, blue: 252.0/255.0, alpha: 1.0)
}
