//
//  DateExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 2/11/2566 BE.
//

import Foundation

extension Date {
    var dateTitle: String {
        let dateFormatter = BaseTool.getDateFormatter(dateFormat: "dd MMM yyyy")
        return dateFormatter.string(from: self)
    }
    
    func getAPIDateFormatString() -> String {
        let format = BaseTool.getDateFormatter()
        return format.string(from: self)
    }
}
