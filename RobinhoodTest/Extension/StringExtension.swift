//
//  StringExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import CryptoKit
import UIKit

extension String {
    var date: Date {
        let isoDate = self
        if let date = BaseTool.getDateFormatter().date(from: isoDate) {
            return date
        } else if let date = BaseTool.getDateFormatter(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").date(from: isoDate) { // for support API over 1 format
            return date
        }
        return Date()
    }
    
    var isToday: Bool {
        let isoDate = self
        if let date = BaseTool.getDateFormatter().date(from: isoDate) {
            return BaseTool.getCalendar().isDateInToday(date)
        } else if let date = BaseTool.getDateFormatter(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").date(from: isoDate) {
            return BaseTool.getCalendar().isDateInToday(date)
        }
        return false
    }
    
    var isTomorrow: Bool {
        let isoDate = self
        if let date = BaseTool.getDateFormatter().date(from: isoDate) {
            return BaseTool.getCalendar().isDateInTomorrow(date)
        } else if let date = BaseTool.getDateFormatter(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").date(from: isoDate) {
            return BaseTool.getCalendar().isDateInTomorrow(date)
        }
        return false
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func hashWithSHA256() -> String {
        let inputString = self
        let inputData = Data(inputString.utf8)
        let hashed = SHA256.hash(data: inputData)
        let hashString = hashed.compactMap { String(format: "%02x", $0) }.joined()
        return hashString
    }
}

