//
//  UIFontExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import UIKit

extension UIFont {
    static func promptBold(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Prompt-Bold", size: size) {
            return font
        }
        return getBaseFont(size: size)
    }
    
    static func promptMedium(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Prompt-Medium", size: size) {
            return font
        }
        return getBaseFont(size: size)
    }
    
    static func promptRegular(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Prompt-Regular", size: size) {
            return font
        }
        return getBaseFont(size: size)
    }
    
    static func getBaseFont(size: CGFloat) -> UIFont {
        return .systemFont(ofSize: size)
    }
}
