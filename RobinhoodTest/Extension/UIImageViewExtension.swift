//
//  UIImageViewExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import Kingfisher
import UIKit

extension UIImageView {
    func setImage(with urlStr: String, placeholder: Placeholder? = nil) {
        if let url = URL(string: urlStr) {
            kf.indicatorType = .activity
            kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.3))], completionHandler: { [weak self] result in
                if case .failure(_) = result {
                    self?.kf.setImage(with: url, placeholder: placeholder)
                }
            })
        }
    }
}
