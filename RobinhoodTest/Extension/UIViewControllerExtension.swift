//
//  UIViewControllerExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 7/11/2566 BE.
//

import UIKit

extension UIViewController {
    func presentFull(controller: UIViewController) {
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true)
    }
}
