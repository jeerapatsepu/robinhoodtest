//
//  UILabelExtension.swift
//  RobinhoodTest
//
//  Created by Jeerapat Sripumngoen on 31/10/2566 BE.
//

import UIKit

extension UILabel {
    func setupLabel(text: String, font: UIFont, color: UIColor, lines: Int = 1) {
        self.text = text
        self.font = font
        numberOfLines = lines
        textColor = color
    }
}
