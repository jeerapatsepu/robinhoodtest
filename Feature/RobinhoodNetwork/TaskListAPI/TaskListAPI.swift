//
//  TaskListAPI.swift
//  RobinhoodNetwork
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import Alamofire
import Foundation

public class TaskListAPI {
    public init() {
        
    }
    
    public func getTaskList(params: [String: Any]? = nil, completion: @escaping (Result<Data, Error>) -> Void) {
        let url = Endpoints.Task.getTaskList
        NetworkManager.shared.fetch(url: url, params: params) { response in
            switch response.result {
                case let .success(data):
                    completion(.success(data))
                
                case let .failure(error):
                    completion(.failure(error))
            }
        }
    }
}
