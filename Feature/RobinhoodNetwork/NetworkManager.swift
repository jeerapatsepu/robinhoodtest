//
//  NetworkManager.swift
//  RobinhoodNetwork
//
//  Created by Jeerapat Sripumngoen on 1/11/2566 BE.
//

import Alamofire
import Foundation

final class NetworkManager {
    static let shared = NetworkManager()
    
    private init() {
        print("init NetworkManager")
    }
    
    func fetch(
        url: String,
        params: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        caching: ResponseCacher = .doNotCache,
        completion: @escaping (AFDataResponse<Data>) -> Void
    ) {
        AF.request(url, method: .get, parameters: params, headers: headers)
            .cacheResponse(using: caching)
            .responseData { response in
                completion(response)
            }
    }
}
