//
//  Endpoints.swift
//  RobinhoodNetwork
//
//  Created by Jeerapat Sripumngoen on 2/11/2566 BE.
//

import Foundation

struct Endpoints {
    static let baseURL = "https://todo-list-api-mfchjooefq-as.a.run.app"
    
    struct Task {
        static let getTaskList = baseURL + "/todo-list"
    }
}
